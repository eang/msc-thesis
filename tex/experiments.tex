\chapter{Experimental Setup}

As part of this work, a real RFID setup has been deployed for learning and experimental purposes. The following chapter describes how the system works and what we did with it.

\section{Tags}

The tags used in the experiments are Umich Moo \cite{zhang2011moo} samples.
They are innovative RFID tags featuring an onboard temperature sensor, accelerometer and LED.
It is also possible to add additional sensors.
These sensors enable many deployment scenarios that would not be possible with simpler RFID tags, such as ultra-low power sensing networks.

\begin{wrapfigure}[16]{R}{0.5\textwidth}
    \framepic[width=0.45\textwidth]{umich_moo.jpg}
    \caption{An Umich Moo RFID tag}
    \label{fig:umich_moo}
\end{wrapfigure}

Figure \ref{fig:umich_moo} shows an example of Moo tag: the antenna resembles the horns of a longhorn steer (which is why the device is called Moo).

The Moo tag is powered by a MSP430 microcontroller from Texas Instruments, an ultra low-power 16-bit CPU.
Note that the onboard sensors are analog, so the Moo has a 12 bit ADC (\emph{Analog-to-Digital Converter}) and a 12 bit DAC (\emph{Digital-to-Analog Converter}) for the conversion between the analog signal from the sensors and the digital signal from the CPU.

The tag is programmable with the aid of a USB FET debugger. All the tags used in the experiments have been programmed with an implementation of the \GenTwo{} standard by the University of Michigan.\footnote{\url{https://github.com/spqr/umichmoo}}. We also used a simple test program that just blinks the onboard LED whenever the tag is powered up by the CW of the reader.

We used the IAR Embedded Workbench IDE to build the software and to flash it onto the tags. \footnote{\url{https://www.iar.com/iar-embedded-workbench/}}

\section{Reader}

The RFID reader that was used in the deployment was an Universal Software Radio Peripheral\texttrademark{} (model USRP1)  by Ettus Research\texttrademark{} (figure \ref{fig:usrp_reader}).
It is made by a main board plus two Flex900/RFX900 daughterboards. Each daughterboard provides an RF bandwidth of 40 MHz and a frequency range from 750 to 1050 MHz.
They both have a main RX/TX connector and an additional RX connector (RX2). The RX/TX connector is limited by an hardware filter to the \SIrange{902}{928}{} MHz ISM band (while the \GenTwo{} frequency range is \SIrange{860}{960}{} MHz \cite{standard1global}).
The software side of the reader is implemented through the GNU Radio\footnote{\url{https://gnuradio.org}} toolkit \cite{buettner2011software}.

\begin{wrapfigure}{R}{0.5\textwidth}
    \framepic[width=0.48\textwidth]{usrp_reader.jpg}
    \caption{The USRP1 reader and the two RFX900 daughterboards.}
    \label{fig:usrp_reader}
\end{wrapfigure}

Figure \ref{fig:overall_system} shows the overall experimental setup.
The two RX/TX connectors on the daughterboards are connected to the two big white antennas (two Cushcraft S9028PCLW models) through SMA cables.
Note that the system uses one antenna for receiving and the other one for transmitting.
There is one tag over one antenna and another tag connected to the FET debugger, which is connected to the right laptop through another USB cable.
The USRP itself is connected to the left laptop through a USB 2.0 cable. It is worth mentioning that the USB 2.0 interface acts as a bottleneck for the bandwidth: even though the daughterboards provides a 40 MHz bandwidth, the overall system bandwidth is much smaller (6 MHz according to \cite{buettner2008empirical}).

\begin{figure}
    \centering
    \framepic[width=0.7\textwidth]{overall_system.jpg}
    \caption{The overall experimental setup}
    \label{fig:overall_system}
\end{figure}

\subsection{GNU Radio}
The reader implementation used by the USRP has been deployed through GNU Radio.
GNU Radio is a framework for \emph{Software Defined Radio} systems, i.e. systems where the signal processing is implemented via software rather than in the hardware.
This flexibility helps reducing the costs of experimental setups, both in money (a consumer computer may be enough) and time.
GNU Radio applications are basically \emph{flow graphs} made by a number of modules connected to each other.
Each module takes a number of signals, processes them and then outputs another number of signals, which become the input of the next module(s) in the flow graph.
Those modules can be built-in modules (part of GNU Radio itself) or custom modules, written from scratch.
Modules are written in C++ but are mostly used from Python applications (thanks to the SWIG\footnote{\url{http://swig.org}} tool that generates Python bindings from C++ headers).
Most Python applications usually just create instances of modules and connect them to build the flow graph.

\subsection{USRP drivers}

GNU Radio needs the \emph{USRP Hardware Driver} (UHD\texttrademark) in order to communicate with the USRP hardware. This is the official driver released by Ettus with an open license.

\subsection{\GenTwo{} standard}

The \GenTwo{} standard has been implemented as a custom GNU Radio module \cite{buettner2011software}. The main challenge faced by the authors was meeting the strict timing requirements enforced by the \GenTwo{} standard: if the reader transmits a command too soon or too late, the tags will ignore it. This required a careful design and  aggressive optimizations to reduce the latencies introduced by the USB interface (due the default block-size of the Linux kernel) and by the default GNU Radio flow graph scheduling.

\subsection{System installation}

The reader implementation has been tested only against a specific version of GNU Radio, on the Ubuntu 10.10 Linux distribution.
The software does not compile on modern Linux systems with recent versions of GNU Radio.
\footnote{A proof-of-concept port is available at \url{https://github.com/elvisangelaccio/QuarkNet/tree/port} but it's not working yet.}
Due to the strict timing requirements discussed above, it's also not possible to run the reader software on a virtualized Ubuntu 10.10 image, because the latency would become too high.
In practice, this is the reason why we used two laptops, since one of them was dedicated to the reader as it had to run a native version of Ubuntu 10.10.
It was also necessary to build the required version of GNU Radio from source (same with the UHD driver).
The detailed installation instructions are available in appendix \ref{chap:instructions}.

\subsection{Cycles}

The reader implementation defines the concept of cycles.
A \emph{cycle} is composed by either:

\begin{itemize}
 \item a fixed number of \emph{rounds} (by default 1), or:
 \item as many rounds as needed to read all the tags
\end{itemize}

A round represents a single \emph{Query} command sent to the tags. In the first configuration (which is the default), the reader powers down after the last cycle ends (there are \num{10000} cycles by default). In the second case, there is only one cycle which ends after all tags have been identified (where the size of the tags population is hard-coded into the reader).


\subsection{Numbers of slots}

The default reader implementation supports either a fixed number of slots, by hardcoding the value of $Q$ (default is 0) as specified by the \GenTwo{} standard, or a variable number of slots which depends on the number of previous reads. This reflects the possible cycle configurations described above.
In our experiments, we used the default configuration (i.e. reader powers down after \num{10000} queries).

\section{Findings}

This section describes what was discovered as part of the experiments.

\subsection{Power required by Moo tags}

One of the first issues encountered was the inability to transmit if tags were too distant from the reader's antennas. In particular, The Moo tags were able to backscatter the signal  only if:

\begin{itemize}
 \item the distance from the antennas was at most 15 centimeters.
 \item the tags were facing the antennas from a specific side.
\end{itemize}

This is probably due to a lack of power necessary to perform the backscattering. Indeed we used a multimeter to measure the voltage of tags while they were backscattering and while they were not. We found out that the voltage was between 1.5 V and 2 V (respectively, for blinking the LED and for the \GenTwo{} Query-Ack exchange) when the backscattering was working, while it was almost negligible (0.2 V) when it was not working.

A possible explanation for this scenario is the presence of \emph{noise}.
Indeed the frequency range used by the USRP (\SIrange{902}{928}{} MHz) is reserved for the Americas, so it is quite possible to get interference of other signals in the same frequency range (for example, GSM in Europe).
We will use the generic term \emph{noise} to refer to either of background noise and interference, since we only care about the loss of power in the CW which makes the communication between reader and tags impossible.
As consequence of this finding, in chapter \ref{chap:noise} we will present an analytical model where we evaluate the impact of noise on the performance of the main sequential protocols.

\subsection{Problems with multi-antenna communication}

The initial goal of the experiment was to evaluate whether the USRP reader could be used to setup a multi-antenna RFID system and implement one or more concurrent protocols. However, it soon became clear that the technical challenges were too difficult and out of the scope of this work. In particular, the USRP1 reader which was available for the experiments is not able to receive multiple signals at the same time from more than one antenna, even though the reader does have a second RX connector on the board (see photo in \ref{fig:usrp_reader}). Another issue was the lack of drivers in GNU Radio for the multiplexing of signals.

\subsection{Multiple tags communication}

\begin{lstlisting}[label=lst:multiple-tags,caption=Excerpt of reader logs showing two different EPC values,float,basicstyle=\small\ttfamily,frame=single,numbers=none]
42852.284,EPC,0,00000001000000000013121A,22.230801
42852.284,QREP,0,0,0
42852.285,EMPTY,0,0,0.816658
42852.286,START_CYCLE,0,0,0
42852.286,QUERY,0,0,0
42852.290,EMPTY,0,0,1.042018
42852.290,QREP,0,0,0
42852.292,EMPTY,0,0,0.848218
42852.296,START_CYCLE,0,0,0
42852.296,QUERY,0,0,0
42852.300,RN16,1,0003,6.071759
42852.300,ACK,0,0,0
42852.304,EPC,1,01010101010101010113121A,6.078180
\end{lstlisting}

As we mentioned before, the reader implementation supports communication with a single RFID tag because the default value of $Q$ is hardcoded to 0 and the \GenTwo{} frame size is $2^Q$ slots.
This was easy to confirm with a simple experiment: while a tag was communication with the reader, we moved another tag within the antenna range.
This reliably caused a collision resulting in the first tag no longer transmitting its EPC value.
As soon as the second tag was removed, the first tag would resume its communication.

We patched and recompiled the reader source code in order to test communication with multiple tags.
The patch was as simple as changing the hardcoded value of $Q$ from 0 to 1.

We then performed a test in which two tags within the communication range successfully transmitted their EPC value to the reader.
To do so, we changed the EPC value in one of the two tags, so that we could distinguish the two values received by the reader.

In particular, the default 96 bits of the EPC values were initialized with the following 12 octets (values are in hexadecimal):

\[
\texttt{0x0 0x0 0x0 0x1 0x0 0x0 0x0 0x0 0x0 0x13 0x12 0x1A}
\]

while our custom EPC value was the following:

\[
\texttt{0x1 0x1 0x1 0x1 0x1 0x1 0x1 0x1 0x1 0x13 0x12 0x1A}
\]
The excerpt from the URSP logs in \ref{lst:multiple-tags} shows both the default EPC value (first line) and the patched EPC value (last line).



