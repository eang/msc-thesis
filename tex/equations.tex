\chapter{Implementation of the equations}
\label{chap:equations}

In order to implement the recursive equations from chapter \ref{chap:noise} with a numerical or general-purpose programming language, it is necessary to prevent endless recursions.
An endless recursion happens whenever the value of $X(n)$ depends on $X(n)$ itself.

This can be avoided by rewriting the equations so that $X(n)$ appears only on one side of the equation.

This appendix also contains Python snippets with the implementation of the most relevant equations.\footnote{We omit the implementation of $X_{idle}$, since it's not necessary for the metrics we used.}
The full code developed as part of this work is available online.\footnote{\url{https://gitlab.com/eang/msc-thesis}}

\section{\FSA{} equations}

We have an endless recursion only in the $n=1$ base case.

Listings \ref{lst:fsa-X_n} and \ref{lst:fsa-X_id_n} show respectively the total number of slots and the total number of identification slots in \FSA{}.

\begin{equation*}
X(1) = s(1) + X(1)\dataerr = \frac{s(1)}{1 - \dataerr}
\end{equation*}

% NOTE: here only as reference, we omit the Python code for X_idle.
% \begin{align*}
% X_{idle}(1) &= A_0^{1,s(1)} + X_{idle}(1)\dataerr \\
% &= s(1) - 1 + \preambleerr + X_{idle}(1)\dataerr \\
% &= \frac{s(1) - 1 + \preambleerr}{1 - \dataerr}
% \end{align*}

\begin{align*}
X_{id}(1) &= A_1^{1,s(1)} + X_{id}(1)\dataerr \\
&= 1 - \preambleerr - \dataerr + X_{id}(1)\dataerr \\
&= \frac{1 - \preambleerr - \dataerr}{1 - \dataerr}
\end{align*}

\begin{lstlisting}[language=python, caption=Python implementation of total number of slots in \FSA{}, label=lst:fsa-X_n]
def X(n, P_pre, P_data, alpha):
    N = s(n, alpha)
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if P_err == 1 or n == 0:
        return N

    if n == 1:
        return N / (1 - P_data)

    A_1 = n * ((1 - 1/N)**(n-1)) * P_no_err

    if A_1 < 1:
        return N

    return N + X(n - int(A_1), P_pre, P_data, alpha)
\end{lstlisting}

\begin{lstlisting}[language=python, caption=Python implementation of total number of identification slots in \FSA{}, label=lst:fsa-X_id_n]
def X_id(n, P_pre, P_data, alpha):
    N = s(n, alpha)
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if P_err == 1 or n == 0:
        return 0

    if n == 1:
        return (1 - P_pre - P_data) / (1 - P_data)

    A_1 = n * ((1 - 1/N)**(n-1)) * P_no_err

    if A_1 < 1:
        return 0

    return int(A_1) + X_id(n - int(A_1), P_pre, P_data, alpha)
\end{lstlisting}

\section{\TSA{} equations}

We have an endless recursion if $n=1$ but also if $n>1$ and $k=n$.

The Python snippets in \ref{lst:tsa-X_n} and \ref{lst:tsa-X_id_n} shows how we compute the total number of slots and identification slots in \TSA{}.

We don't include the definition of the \texttt{p(N, n, k)} function, which is simply the implementation of equation \ref{eq:pmf}.

An important note about the Python implementation is that Python does not offer \emph{memoization} of recursive functions.
Since in \TSA{} (but also in \QT{}) we call $X(n)$ multiple times with the same argument, we implemented our own memoization framework so that we don't compute the same value over and over again.

\begin{align*}
X(n) &= s(n) + a_1^{n,s(n)} X(1)\dataerr \\
& \quad + s(n) \sum_{k=2}^n p(s(n),n,k) X(k)(1- \preambleerr) \\
&= s(n) + a_1^{n,s(n)} X(1)\dataerr + s(n)\left(\frac{1}{s(n)}\right)^n X(n)(1-\preambleerr) \\
& \quad + s(n) \sum_{k=2}^{n-1} p(s(n),n,k) X(k)(1- \preambleerr) \\
&= \frac{s(n) + a_1^{n,s(n)} X(1)\dataerr + s(n) \sum\limits_{k=2}^{n-1} p(s(n),n,k) X(k)(1- \preambleerr)}{1 - s(n)\left(\frac{1}{s(n)}\right)^n (1-\preambleerr)}
\end{align*}
In the $n=1$ case we get:
\[
 X(1) = s(1) + X(1)\dataerr = \frac{s(1)}{1-\dataerr}
\]

\begin{lstlisting}[language=python, caption=Python implementation of total number of slots in \TSA{}, label=lst:tsa-X_n]
def X(n, P_pre, P_data, alpha):
    N = s(n, alpha)
    P_err = P_pre + P_data

    if P_err == 1 or n == 0:
        return N

    if n == 1:
        return N / (1 - P_data)

    sum = 0
    for k in range(2,n):
        sum += p(N, n, k)*(1-P_pre)*X(k, P_pre, P_data, alpha)

    return (N + (n*(1-1/N)**(n-1)*P_data*X(1,P_pre,P_data,alpha))
            + N*sum) / (1 - N*(1/N)**n*(1 - P_pre))
\end{lstlisting}

% NOTE: here only as reference, we omit the Python code for X_idle.
% Let $Y=X_{idle}$.
% \begin{align*}
% Y(n) &= A_0^{n,s(n)} + a_1^{n,s(n)} Y(1)\dataerr \\
% & \quad + s(n) \sum_{k=2}^n p(s(n),n,k) Y(k)(1- \preambleerr) \\
% &= A_0^{n,s(n)} + a_1^{n,s(n)} Y(1)\dataerr + s(n)\left(\frac{1}{s(n)}\right)^n Y(n)(1-\preambleerr) \\
% & \quad + s(n)(1- \preambleerr) \sum_{k=2}^{n-1} p(s(n),n,k) Y(k) \\
% &= \frac{A_0^{n,s(n)} + a_1^{n,s(n)} Y(1)\dataerr + s(n)(1- \preambleerr) \sum\limits_{k=2}^{n-1} p(s(n),n,k) Y(k)}{1 - s(n)\left(\frac{1}{s(n)}\right)^n (1-\preambleerr)}
% \end{align*}
% In the $n=1$ case we get:
% \[
%  X_{idle}(1) = A_0^{1,s(1)} + X_{idle}(1)\dataerr = \frac{s(1) - 1 + \preambleerr}{1-\dataerr}
% \]





\begin{align*}
X_{id}(n) &= A_1^{n,s(n)} + a_1^{n,s(n)} X_{id}(1)\dataerr \\
& \quad + s(n) \sum_{k=2}^n p(s(n),n,k) X_{id}(k)(1- \preambleerr) \\
&= A_1^{n,s(n)} + a_1^{n,s(n)} X_{id}(1)\dataerr + s(n)\left(\frac{1}{s(n)}\right)^n X_{id}(n)(1-\preambleerr) \\
& \quad + s(n) \sum_{k=2}^{n-1} p(s(n),n,k) X_{id}(k)(1- \preambleerr) \\
&= \frac{A_1^{n,s(n)} + a_1^{n,s(n)} X_{id}(1)\dataerr + s(n) \sum\limits_{k=2}^{n-1} p(s(n),n,k) X_{id}(k)(1- \preambleerr)}{1 - s(n)\left(\frac{1}{s(n)}\right)^n (1-\preambleerr)}
\end{align*}
In the $n=1$ case we get:
\[
 X_{id}(1) = A_1^{1,s(1)} + X_{id}(1)\dataerr = \frac{1 - \preambleerr - \dataerr}{1-\dataerr}
\]

\begin{lstlisting}[language=python, caption=Python implementation of total number of identification slots in \TSA{}, label=lst:tsa-X_id_n]
def X_id(n, P_pre, P_data, alpha):
    N = s(n, alpha)
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if P_err == 1 or n == 0:
        return 0

    if n == 1:
        return (1 - P_pre - P_data) / (1 - P_data)

    sum = 0
    for k in range(2,n):
        sum += p(N, n, k)*(1-P_pre)*X(k, P_pre, P_data, alpha)

    A_1 = n * ((1 - 1/N)**(n-1)) * P_no_err

    return (A_1+(n*(1-1/N)**(n-1)*P_data*X_id(1,P_pre,P_data,alpha))
           + N*sum) / (1 - N*(1/N)**n*(1 - P_pre))
\end{lstlisting}


\section{\QT{} equations}

Similar to the \TSA{} equations, but now we have an additional endless recursion if $n>1$ and $k=0$.

The Python implementation of $X(n)$ and $X_{id}(n)$ is shown in listings \ref{lst:qt-X_n} and \ref{lst:qt-X_id_n}.

\begin{lstlisting}[language=python, caption=Python implementation of total number of rounds in \QT{}, label=lst:qt-X_n]
def X(n, P_pre, P_data):
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if n == 0:
        return 1

    if n == 1:
        return 1 / (1 - P_data)

    s1 = 0
    for k in range(1,n):
        s1 += p(2, n, k) * X(k, P_pre, P_data)

    s2 = 0
    for k in range(1,n):
        s2 += p(2, n, k) * X(n-k, P_pre, P_data)

    return (1 + (1/2)**(n-1) * X(0, P_pre, P_data) * (1-P_pre)
           + (1-P_pre)*(s1+s2)) / (1 - (1/2)**(n-1) * (1-P_pre))
\end{lstlisting}

\begin{lstlisting}[language=python, caption=Python implementation of total number of identification rounds in \QT{}, label=lst:qt-X_id_n]
def X_id(n, P_pre, P_data):
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if n == 0:
        return 0

    if n == 1:
        return (1 - P_pre - P_data) / (1 - P_data)

    s1 = 0
    for k in range(1,n):
        s1 += p(2, n, k) * X_id(k, P_pre, P_data)

    s2 = 0
    for k in range(1,n):
        s2 += p(2, n, k) * X_id(n-k, P_pre, P_data)

    return ((1/2)**(n-1) * X_id(0, P_pre, P_data) * (1-P_pre)
           + (1-P_pre)*(s1+s2)) / (1 - (1/2)**(n-1) * (1-P_pre))
\end{lstlisting}

\begin{align*}
X(n) &= 1 + (1-\preambleerr)\sum_{k=0}^n \binmassfunc{2}{n}{k} (X(k) + X(n-k)) \\
&= 1 + 2(1-\preambleerr)\left(\frac{1}{2}\right)^nX(0) + 2(1-\preambleerr)\left(\frac{1}{2}\right)^nX(n) \\
& \quad + (1-\preambleerr)\sum_{k=1}^{n-1} \binmassfunc{2}{n}{k} (X(k) + X(n-k)) \\
&= 1 + (1-\preambleerr)\left(\frac{1}{2}\right)^{n-1} X(0) + (1-\preambleerr)\left(\frac{1}{2}\right)^{n-1} X(n) \\
& \quad + (1-\preambleerr)\sum_{k=1}^{n-1} \binmassfunc{2}{n}{k} (X(k) + X(n-k)) \\
&= \frac{1 + (1-\preambleerr)\left(\frac{1}{2}\right)^{n-1} + (1-\preambleerr)\sum\limits_{k=1}^{n-1} p(2,n,k) (X(k) + X(n-k))}{1 - \left(\frac{1}{2}\right)^{n-1} (1-\preambleerr)}
\end{align*}

In the $n=1$ case we get:

\[
 X(1) = 1 + X(1)\dataerr = \frac{1}{1-\dataerr}
\]

Let $Y=X_{id}$.
\begin{align*}
Y(n) &= (1-\preambleerr)\sum_{k=0}^n \binmassfunc{2}{n}{k} (Y(k) + Y(n-k)) \\
&= 2(1-\preambleerr)\left(\frac{1}{2}\right)^nY(0) + 2(1-\preambleerr)\left(\frac{1}{2}\right)^nY(n) \\
& \quad + (1-\preambleerr)\sum_{k=1}^{n-1} \binmassfunc{2}{n}{k} (Y(k) + Y(n-k)) \\
&= (1-\preambleerr)\left(\frac{1}{2}\right)^{n-1} Y(n) + (1-\preambleerr)\sum_{k=1}^{n-1} p(2,n,k) (Y(k) + Y(n-k)) \\
& \quad + (1-\preambleerr)\sum_{k=1}^{n-1} \binmassfunc{2}{n}{k} (Y(k) + Y(n-k)) \\
&= \frac{(1-\preambleerr)\left(\frac{1}{2}\right)^{n-1} + (1-\preambleerr)\sum\limits_{k=1}^{n-1} p(2,n,k) (Y(k) + X_{id}(n-k))}{1 - \left(\frac{1}{2}\right)^{n-1} (1-\preambleerr)}
\end{align*}

In the $n=1$ case we get:

\[
 X_{id}(1) = 1 - \preambleerr -\dataerr + X_{id}(1)\dataerr = \frac{1-\preambleerr-\dataerr}{1-\dataerr}
\]
