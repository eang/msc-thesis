\chapter{Improving the protocols in presence of noise}

\subsection{Termination of \FSA-based protocols}

In a \FSA-based protocol in a classic noise-free scenario, if there is at least one identification or one collision slot in a frame of size $s(n)$, then there is at least one tag that the reader has not identified yet. As corollary of this observation, if the frame contains \emph{only} idle slots then there are no more tags to be identified and the reader can stop the execution of the protocol.

This \emph{exit condition} does not work in our noise model, because now there may be an idle slot even if there is only one tag left. This means that the reader cannot stop if no tag replies in a frame of size $s(n)$. The reader has to issue another frame instead, because there might be still tags waiting to be identified.

We assume that the reader increments a global counter $\ell$ every time it issues another frame due to the previous frame having only idle slots.
The protocol will end as soon as $\estimateerr > \thresholdend$, where $\estimateerr$ is an estimate of $\anyerr$ computed by the reader, while $\thresholdend$ is a fixed threshold. In practice, the $\thresholdend$ threshold will prevent the presence of endless loops, when there are no tags left or when $\preambleerr \simeq 1$. In other words, we assume the reader is able to estimate \preambleerr{} and abort when it becomes too high.

To summarize, this is what can possibly happen:

\begin{itemize}
 \item Either there is one or more tags, or there are no tags.
 \begin{itemize}
 \item If there are no tags left, the reader will keep increasing $\ell$ and eventually the $\estimateerr > \thresholdend$ exit condition will be true.
 \end{itemize}

 \item Otherwise, either there are only idle slots, or there is at least one non-idle slot.
 \begin{itemize}
 \item If there are only idle slots, it means that $\preambleerr \simeq 1$. This is equivalent to the case with zero tags.
 \item Otherwise, the reader knows that there is at least one tag (which triggered an identification or collision slot). The protocol can go on and there is no reason to increment $\ell$. \end{itemize}

\end{itemize}

% TODO: convert to actual/better pseudocode
%
% FSA:
%
% 0. l = 1, P_err_estimate = 0.01
% 1. Reader issues next frame
% 2. P_err_estimate = P_err_estimate^(1/l)
% 3. If p > P_end, abort
% 4. If no one replies (= only idle slots), increase l and repeat from step 1  // either there are no tags (left), or P_err = 1.
% 5. Go back to step 1.
%
% TSA:
%
% 0. l = 1, P_err_estimate = 0.01
% 1. Reader issues next frame
% 2. P_err_estimate = P_err_estimate^(1/l)
% 3. If p > P_end, abort
% 4. If no one replies (= only idle slots), increase l and repeat from step 1  // either there are no tags (left), or P_err = 1.
% 6. For each collision, issue a child frame recursively until each child frame no longer has collision slots.
% 5. Go back to step 1.



\subsection{Evaluation of the \FSA{} protocol}


The other big difference is the exit condition of the protocol. We also need an additional input parameter for the $\ell$ counter. Note also that in the analytical model we do know the value of $\anyerr$, so we just compare the value of $\ell$ with a fixed upper threshold $\lmax$.

\subsubsection{Total number of slots}


If $\preambleerr = 1$, the equation would enter in an infinite loop because there are zero tags identified and all the $n$ tags would partecipate in the next frame. An infinite loop would also happen if there are no tags ($n=0$). In both cases we need to increment the $\ell$ counter. Note also how we are now using the ceil function rather than the floor function. This is because $A_1^{n,s(n)}$ (unlike $a_1^{n,s(n)}$) can be less than 1 for $n > 1$. Taking its floor value would lead, again, to an endless recursion.
\[
X(n,\ell) =
\begin{cases}
s(n)       & \quad \text{if } \ell > \lmax \\
s(n) +  X(n,\ell+1)  & \quad \text{if } \ell \leq \lmax \text{ and }  n = 0 \\
s(n) +  X(n,\ell+1)  & \quad \text{if } \ell \leq \lmax \text{ and }  \preambleerr = 1 \\
s(n) +  X(n - \ceil{\AVGid},\ell)  & \quad \text{if } \ell \leq \lmax \\
\end{cases}
\]

\subsubsection{Total number of idle slots}



\[
X_{idle}(n,\ell) =
\begin{cases}
s(n)       & \quad \text{if } \ell > \lmax \\
s(n) +  X_{idle}(n,\ell+1)  & \quad \text{if } \ell \leq \lmax \text{ and }  n = 0 \\
s(n) +  X_{idle}(n,\ell+1)  & \quad \text{if } \ell \leq \lmax \text{ and }  \preambleerr = 1 \\
A_0^{n,s(n)} + X_{idle}(n - \ceil{\AVGid},\ell)  & \quad \text{if } \ell \leq \lmax \\
\end{cases}
\]

\subsubsection{Total number of identification slots}



\[
X_{id}(n,\ell) =
\begin{cases}
0       & \quad \text{if } \ell > \lmax \\
X_{id}(n,\ell+1)  & \quad \text{if } \ell \leq \lmax \text{ and }  n = 0 \\
X_{id}(n,\ell+1)  & \quad \text{if } \ell \leq \lmax \text{ and }  \preambleerr = 1 \\
\AVGid \ + \\
X_{id}(n - \ceil{\AVGid},\ell)  & \quad \text{if } \ell \leq \lmax \\
\end{cases}
\]

\subsection{Evaluation of the \TSA{} protocol}


\subsubsection{Total number of slots}


\[
X(n,\ell) =
\begin{cases}
s(n)       & \quad \text{if } \ell > \lmax \\
s(n) + X(n,\ell+1) & \quad \text{if } \ell \leq \lmax \text{ and }  n = 0 \\
s(n) + X(n,\ell+1) & \quad \text{if } \ell \leq \lmax \text{ and }  \preambleerr = 1 \\
s(n) + \avgid \dataerr \cdot X(1,\ell+1) \ + & \quad \text{if } \ell \leq \lmax \\
s(n) \sum_{k=2}^n p(s(n),n,k) (1- \preambleerr) \cdot X(k,\ell)
\end{cases}
\]

\subsubsection{Total number of idle slots}

\[
X_{idle}(n,\ell) =
\begin{cases}
s(n)       & \quad \text{if } \ell > \lmax \\
s(n) + X_{idle}(n,\ell+1) & \quad \text{if } \ell \leq \lmax \text{ and } n = 0 \\
s(n) + X_{idle}(n,\ell+1) & \quad \text{if } \ell \leq \lmax \text{ and }  \preambleerr = 1 \\
A_0^{n,s(n)} + \avgid \dataerr \cdot X_{idle}(1,\ell+1) \ + & \quad \text{if } \ell \leq \lmax \\
+\ s(n) \sum_{k=2}^n p(s(n),n,k) (1- \preambleerr) \cdot X_{idle}(k,\ell)  \\
\end{cases}
\]

\subsubsection{Total number of identification slots}


\[
X_{id}(n,\ell) =
\begin{cases}
0       & \quad \text{if } \ell > \lmax \\
X_{id}(n,\ell+1) & \quad \text{if } \ell \leq \lmax \text{ and } n = 0 \\
X_{id}(n,\ell+1) & \quad \text{if } \ell \leq \lmax \text{ and }  \preambleerr = 1 \\
A_1^{n,s(n)} + \avgid \dataerr \cdot X_{id}(1,\ell+1) \ + & \quad \text{if } \ell \leq \lmax \\
+\ s(n) \sum_{k=2}^n p(s(n),n,k) (1- \preambleerr) \cdot X_{id}(k,\ell)  \\
\end{cases}
\]

\section{Noise Model for tree-based protocols}

\subsection{Evaluation of the \QT{} protocol}

\subsubsection{Total number of rounds}



\subsubsection{Total number of identification rounds}



The \QTl{} protocol \cite{law2000efficient} is a \emph{fault tolerant} variant of \QT. The error model defined by the authors is quite similar to our noise model introduced in section \ref{sec:noise-model-aloha}. In particular, they assume that a tag fails to reply to a query with a probability $p$. The main idea of \QTl{} is that the reader cannot be sure about the outcome of a query $q$ if there are empty or single replies:

\begin{itemize}
 \item If there is a collision, the reader will conclude that two or more tags match the prefix $q$.
 \item Otherwise, the reader sends again the same query $q$ for $\ell$ times (unless it detects a collision). After these $\ell$ queries, the reader checks whether zero, one or more than one tag replied to the query.
\end{itemize}

This shows an important difference with ALOHA-based protocols: if an identification slot becomes idle, the missed tag can still partecipate in the next frame. In the \QT{} protocol instead, if the reader does not repeate the same query multiple times, it could lose some tags along the way.

\subsection{Evaluation of the \texorpdfstring{$\QTl$}{QT\textasciicircum l} protocol}

We begin by evaluating the total number of nodes of the \QTl{} protocol in the error model defined in \cite{law2000efficient}.

The reader can receive either a collision reply or a non-collision type of reply (empty or single). The latter happens with probability:
\[
 p^n + np^{n-1}(1-p)
\]
where $p^n$ is the probability that all tags fail to reply, while $np^{n-1}(1-p)$ is the probability that only one tag manages to reply.


\subsubsection{Total number of nodes}

Let $q = p^n + np^{n-1}(1-p)$. Then the total number of nodes in the \QTl{} protocol is given by:
\[
X(n) =
\begin{cases}
\lmax       & \text{if } n \leq 1 \\
1 + (1-q)\sum_{k=0}^n \binom{n}{k} \left(\frac{1}{2}\right)^{k} \left(\frac{1}{2}\right)^{n-k} (X(k) + X(n-k))\ + & \text{if } n > 1 \\
+\ X(n) \cdot q
\end{cases}
\]
In the base case (zero or one tag), the protocol repeats the same query exactly \lmax{} times.

\subsection{Evaluation of the \QT{} protocol}

In this section we evaluate the \QT{} protocol by using our error model. The resulting protocol is similar to \QTl{}, but there are important differences:

\begin{itemize}
 \item A single reply becomes empty with probability $\preambleerr$, while it becomes a collision with probability $\dataerr$.
 \item A collision reply becomes empty with probability $\preambleerr$.
\end{itemize}

Clearly, an empty reply remains necessarily empty. Let us consider what could possibly happen with a population of $n=3$ tags whose IDs are 010, 011 and 101.
First, the reader issues the query $q = 0$ which would normally result in a collision between tags 010 and 011:

\begin{enumerate}
 \item If there is an error on the preamble, the reader gets an empty reply rather than a collision. The reader cannot conclude that no tags have 0 as prefix (in fact, that is not the case in this example), so it will send again the same query with an increased $\ell$ counter.
 \item If there is an error on the payload, the reader will still get a collision.
 \item If there are no errors, the reader will get a collision as expected.
\end{enumerate}

Then, the reader goes on with the next query ($q=1$):

\begin{enumerate}
 \item If there is an error on the preamble, the reader will get an empty reply rather than a single one. Again, the reader will have to try again this query with a value of $\ell$ increased by one.
 \item If there is an error on the packet data, the single reply will be identified as a collision reply. However, this is not really a problem. There will simply be only tag partecipating in the sub-tree for the collision and it will be eventually identified.
 \item If there are no errors, the reader gets a single reply and it can conclude that there are no other tags with the same prefix. This is probably this biggest difference from the \QTl{} protocol (which would send again the query in this case).
\end{enumerate}



\subsubsection{Total number of nodes}

The main difference from the \QTl{} equation is that now we need a separate case for $n=1$:

\begin{itemize}
 \item If there are two or more tags with the same prefix, we can have either a collision or an empty reply.
 \item If there is only one tag with the same prefix, we can have a single reply (if there are no errors), a collision reply (with probability \dataerr) and an empty reply (with probability \preambleerr).
\end{itemize}
\[
X(n) =
\begin{cases}
\lmax & \text{if } n = 0 \\
\lmax & \text{if } \preambleerr = 1 \\
1 + X(1)\cdot\preambleerr + (X(1)+X(0))\cdot\dataerr & \text{if } n = 1 \\
1 + \sum_{k=0}^n \binom{n}{k} \left(\frac{1}{2}\right)^{k} \left(\frac{1}{2}\right)^{n-k} (X(k,1) + X(n-k,1))(1-\preambleerr)\ + & \text{if } n > 1 \\
+\ X(n,\ell+1) \cdot \preambleerr
\end{cases}
\]

\section{System Efficiency}



\subsection{Influence of probability of errors}






\subsection{Influence of exit threshold}
\label{sec:se_noise_exit}

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color]
    set datafile separator ','
    set xlabel '$\lmax$'
    set ylabel 'System Efficiency'
    set xrange [5:50]
    set xtics 5
    set yrange [0.0:0.75]
    set ytics 0.05
    set style line 1 pt 4
    set style line 2 pt 5
    set style line 3 pt 10 lc rgb 'dark-yellow'
    set style line 4 pt 13 lc rgb 'brown'
    set style line 5 pt 11 lc rgb 'dark-cyan'
    set style line 6 pt 12 lc rgb 'orange-red'
    set style line 7 pt 7 lc rgb 'slateblue1'
    set style line 8 pt 9 lc rgb 'blue'
    plot 'csv/fsa_se_noise_exit_0.30.csv' using 1:2 with linespoint ls 1 title '\FSA{} ($\anyerr=0.30$)', 'csv/tsa_se_noise_exit_0.30.csv' using 1:2 with linespoint ls 2 title '\TSA{} ($\anyerr=0.30$)', 'csv/fsa_se_noise_exit_0.50.csv' using 1:2 with linespoint ls 3 title '\FSA{} ($\anyerr=0.50$)', 'csv/tsa_se_noise_exit_0.50.csv' using 1:2 with linespoint ls 4 title '\TSA{} ($\anyerr=0.50$)', 'csv/qtl_se_noise_exit_0.30.csv' using 1:2 with linespoint ls 6 title '\QTl{} ($\anyerr=0.30$)', 'csv/qt_se_noise_exit_0.50.csv' using 1:2 with linespoint ls 7 title '\QT{} ($\anyerr=0.50$)', 'csv/qt_se_noise_exit_0.30.csv' using 1:2 with linespoint ls 8 title '\QT{} ($\anyerr=0.30$)'
    \end{gnuplot}
    \caption{System Efficiency comparison by varying the exit threshold $\lmax$ $(n=500, \preambleerr=0.15 \cdot \anyerr)$.}
    \label{fig:system-efficiency-noise-exit}
\end{figure}

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color]
    set datafile separator ','
    set ylabel 'System Efficiency'
    set yrange [0:0.5]
    set ytics 0.05
    set boxwidth 0.7
    set style data histograms
    set style fill solid border -1
    plot 'csv/se_comparison_exit.csv' using 2:xtic(1) title 'Classic', '' using 3 title '$\lmax=5$', '' using 4 title '$\lmax=10$', '' using 5 title '$\lmax=15$', '' using 6 title '$\lmax=30$'
    \end{gnuplot}
    \caption{System Efficiency comparison between classic and noise models with $n = 500 $ and $\anyerr=0.3$.}
    \label{fig:system-efficiency-comparison-exit}
\end{figure}



\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color]
    set datafile separator ','
    set xlabel 'Tags'
    set ylabel 'System Efficiency'
    set yrange [0.25:0.5]
    set ytics 0.05
    set boxwidth 0.7
    set style data histograms
    plot 'csv/tsa_se.csv' using 2:xticlabels(1) fs solid 0.7 border lt -1 title '\TSA{} (classic)', 'csv/tsa_se_zero_noise.csv' using 2 fs empty title '\TSA{} ($\anyerr=0$)', 'csv/fsa_se.csv' using 2 fs pattern 12 title '\FSA{} (classic)', 'csv/fsa_se_zero_noise.csv' using 2 fs pattern 10 title '\FSA{} ($\anyerr=0$)'
    \end{gnuplot}
    \caption{System Efficiency comparison between classic model and noise model with $\anyerr=0$ and $\lmax=50$.}
    \label{fig:system-efficiency-zero-noise}
\end{figure}

In figure (\ref{fig:system-efficiency-noise-exit}) we can analyze how the SE depends on the value of $\lmax$. Again, we ran the simulation over 500 tags.
For the \FSA{} protocol, the SE decreasing is \emph{almost} negligible.
However, for \TSA{} it's actually negligible. This happens because $X(1,\ell)$ is a geometric series (for $s(1)=1$):

\begin{align*}
X(1,\ell) &= 1 + \dataerr \cdot X(1, \ell+1) \\
&= 1 + \dataerr (1 + \dataerr \cdot X(1, \ell+2)) \\
& \vdotswithin{=} \\
&= \sum_{k=0}^{\lmax} \dataerr^k
\end{align*}
Since $\dataerr < 1$, this is a convergent series:

$$ \sum_{k=0}^{\lmax} \dataerr^k = \frac{1}{1-\dataerr} $$

Finally, \QT{} and \QTl{} are the protocols more affected by higher values of \lmax. Even $\lmax=10$ is already too much, resulting in a very low System Efficiency.

Even in this scenario we compared the \FSA{}, \TSA{} and \QT{} protocols with their noise-free version.
As can be seen in figure \ref{fig:system-efficiency-comparison-exit}, \QT{} is the most affected by higher values of \lmax{}.

\subsection{Comparison with noise-free model}



Another interesting experiment was the reduction of the noise model to the classic noise-free model.
To do so, we set $\anyerr = 0$ and we ran again our analytical simulations.
The results are shown in figure \ref{fig:system-efficiency-zero-noise}.
We did not include the \QT{} and \QTl{} protocols in this figure, since it's clear that we can never reach the same performance of the classic \QT{} (even if $\anyerr=0$, the reader would still repeat the queries \lmax{} times).
We did include \FSA{} even though is affected by a similar problem: even if $\anyerr=0$ and there is only one tag left, the reader would still continue to issue another frame until we reach the exit threshold.
The \TSA{} protocol instead is not affected by this problem and shows the same performance as the classic \TSA{}: this happens because it's not possible to reach a scenario where there is only one tag left (if $\anyerr=0$). %%% TODO: explain it better.


\section{Percentage of identification}



\subsection{Influence of exit threshold}

We repeated the same comparison by varying \lmax{} rather than \anyerr{} (figure \ref{fig:identification-comparison-exit}). These results are consistent with those we discussed in section \ref{sec:se_noise_exit}.

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color]
    set datafile separator ','
    set key outside horizontal
    set ylabel 'Percentage of identification ($X_{id}(n) / n$)'
    set yrange [0.8:1]
    set ytics 0.05
    set boxwidth 0.7
    set style data histograms
    set style fill solid border -1
    plot 'csv/id_comparison_exit.csv' using 2:xtic(1) title 'Classic', '' using 3 title '$\lmax=5$', '' using 4 title '$\lmax=10$', '' using 5 title '$\lmax=15$', '' using 6 title '$\lmax=30$'
    \end{gnuplot}
    \caption{Comparison of the percentage of identification between classic and noise models ($n = 500 $ and $\anyerr=0.3$).}
    \label{fig:identification-comparison-exit}
\end{figure}

