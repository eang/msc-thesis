\chapter{Analytical Estimation of Performance}
\label{chap:performance}

This chapter will present a performance evaluation of the sequential protocols described in chapter \ref{chap:sequentials}. The performance will be estimated using an analytical technique, where we model each protocol in a realistic scenario and we compute the metrics of interests, such as the System Efficiency.

\section{ALOHA-based protocols}

To estimate the latency of an ALOHA-based protocol, we need an analytic model where we can count how many slots the protocol uses to identify all the tags. Once we know this number, we can just derive the latency by using a realistic model for the transmission time.

\subsection{Transmission Time Model}

We use a transmission time model based on the \GenTwo{} standard and assume a bit rate of 40 kbps. Let $T_n$ be the time required to transmit $n$ bit and $T_s$ the time required to transmit a slot of type $s$ ($s$ can be $id, idle,$ or $coll$). According to our data rate we have that $ T_1 = 25 \mu s$.

This model also needs to account for the \emph{physical overhead}, which includes the time to transmit the preamble, the propagation delay and the reaction time (which is a small time between the receiving phase and the sending phase).
Let $T_{phy}$ be such overhead. In practice, we have:

\[
  T_s = T_{phy} + T_{\#bits\ in\ s}
\]

\subsection{Metrics}

We now formally define the metrics of interest for ALOHA-based protocols.

\subsubsection{Latency}

Let $T(n)$ be the time needed to identify $n$ tags, that is the latency of the protocol. Let also $X_s(n)$ be the number of slots of type $s$ used for identifying $n$ tags. Then we can define $T(n)$ as:

\[
 T(n) = X_{id}(n) T_{id} + X_{coll}(n) T_{coll} + X_{idle}(n) T_{idle}
\]
We already know that identification and collision slots last the same amount of time ($T_{id} = T_{coll}$), which means:

\begin{equation}
\label{eq:latency}
 T(n) = (X_{id}(n) + X_{coll}(n)) T_{id/coll} + X_{idle}(n) T_{idle}
\end{equation}

\subsubsection{System Efficiency}

It is very easy to compute the System Efficiency (\ref{eq:se}) once we know the total number of slots executed by the protocol:

\begin{equation}
\label{eq:se_n}
 SE(n) = \frac{X_{id}(n)}{X_{idle}(n) + X_{id}(n) + X_{coll}(n)}
\end{equation}

\subsubsection{Time System Efficiency}

The formula \ref{eq:tse} introduced in chapter \ref{chap:sequentials} can now be rewritten in the following way:

\begin{equation}
\label{eq:tse_n}
 TSE(n) = \frac{X_{id}(n)}{\beta X_{idle}(n) + X_{id}(n) + X_{coll}(n)}
\end{equation}
However, we will not compute the TSE in this analysis and we will focus on the SE instead.

\subsection{Evaluation of the \FSA{} protocol}

We begin by estimating the number of slots of the \FSA{} protocol.
We have already seen in \ref{eq:expected_slots} that the expected number of slots is given by the binomial distribution. Let $p(s(n),n,k)$ be the probability mass function of a random variable $X \sim Bin(n, \frac{1}{s(n)})$, i.e. the probability that $k$ tags out of $n$ choose the same slot (among the $s(n)$ available ones).

\begin{equation}
\label{eq:pmf}
 p(s(n),n,k) = f_X(k) = \binmassfunc{s(n)}{n}{k}
\end{equation}

This means that is trivial to compute the expected number of, respectively, idle and identification slots in a frame of size $s(n)$:

\begin{align*}
 a_0^{n,s(n)} &= s(n) p(s(n),n,0) = \avgidle \\
 a_1^{n,s(n)} &= s(n) p(s(n),n,1) \\
 &= s(n) n \left(\frac{1}{s(n)}\right) \left(1 - \frac{1}{s(n)}\right)^n \\
 &= \avgid
\end{align*}
The expected number of collision slots in the frame is given by the number of slots with two or more collisions:

\[
 a_{k \geq 2}^{n,s(n)} = s(n) \sum_{k=2}^n p(s(n),n,k) = \avgcoll
\]
Now we have everything we need to estimate the \emph{total} number of slots in the protocol.

\subsubsection{Total number of slots}
% N.B. alpha > 0 garantisce che s(n) > 0, poichè n >= 1.
Let $s(n) = \alpha n$ (with $\alpha > 0$) be the size of the first frame executed by the protocol. Recall that $s(n)$ depends on the (unknown) number of tags $n$ (\ref{eq:chebyshev}).
Let $X(n) = X_{id}(n) + X_{idle}(n) + X_{coll}(n)$, i.e. the total number of slots executed by the protocol to identify $n$ tags. Then this number is given by the number of slots in the first frame plus the number of slots in all the following frames:

\[
X(n) =
\begin{cases}
s(n)       & \quad \text{if } n \leq 1 \\
s(n) +  X(n - \floor{\avgid})  & \quad \text{if } n > 1 \\
\end{cases}
\]
The equation is recursively defined because the only thing we know is $a_1^{n,s(n)}$, the expected number of identified tags in the first frame. Thus $n - \floor{\avgid}$ is the number of tags that will participate in the following frame executed by the protocol.
\footnote{The input of $X()$ must be a natural number, which is why we use the floor function. We could also have used the ceil function, but that's not necessary since $n > 1$ ensures that $\avgid >= 1$ (in other words, there cannot be an endless recursion). }
In the base case instead, we have no collisions and no next frame, since there is at most one tag.

\subsubsection{Total number of idle slots}
As we know, idle slots last less time than non-idle slots. Thus it is useful to computed their number separately. As before, it is given by the number of idle slots in the first frame plus the number of idle slots in all the following frames:

\[
X_{idle}(n) =
\begin{cases}
s(n)       & \quad \text{if } n = 0 \\
s(n)-1       & \quad \text{if } n = 1 \\
\avgidle + X_{idle}(n - \floor{\avgid})  & \quad \text{if } n > 1 \\
\end{cases}
\]

\subsubsection{Latency}
We already have everything we need to compute the latency as in \ref{eq:latency}, since $X(n) - X_{idle}(n) = X_{id}(n) + X_{coll}(n)$.

\subsubsection{SE}
To compute $SE(n)$ (\ref{eq:se_n}) we also need the total number of identification slots:

\[
X_{id}(n) =
\begin{cases}
n       & \quad \text{if } n \leq 1 \\
\avgid + X_{id}(n - \floor{\avgid})  & \quad \text{if } n > 1 \\
\end{cases}
\]
Note that this formula is only useful to prove that the analytic framework is formally correct, as we always expect (without computing it) that $X_{id}(n) = n$.


\subsection{Evaluation of the \TSA{} protocol}
\label{sec:tsa-classic-evaluation}

\subsubsection{Total number of slots}
As for \FSA{}, let $s(n) = \alpha n$ be the size of the root frame in a \TSA{} tree and let $X(n) = X_{id}(n) + X_{idle}(n) + X_{coll}(n)$. Then the total number of slots is given by the number of slots in the root frame plus the number of slots in all the children frames.
Since we have one child for each collision in the parent frame, we get:

\[
X(n) =
\begin{cases}
s(n)       & \quad \text{if } n \leq 1 \\
s(n) + \avgcoll X(k)  & \quad \text{if } n > 1 \\
\end{cases}
\]
$X(n)$ is recursively defined because the expected number of collision slots in the root frame is $a_{2 \leq k \leq n}^{n,s(n)}$, each of which issues a child tree whose total number of slots is exactly $X(k)$. In the base case instead, we have no collisions and no child frames, since there is at most one tag.

\subsubsection{Total number of idle slots}
As before, it is given by the number of idle slots in the root frame plus the number of idle slots in all the children:

\[
X_{idle}(n) =
\begin{cases}
s(n)       & \quad \text{if } n = 0 \\
s(n)-1       & \quad \text{if } n = 1 \\
a_0^{n,s(n)} + \avgcoll X_{idle}(k)  & \quad \text{if } n > 1 \\
\end{cases}
\]

\subsubsection{Total number of identification slots}
As we did for \FSA{}, to compute $TSE(n)$ we also need the total number of identification slots:

\[
X_{id}(n) =
\begin{cases}
n       & \quad \text{if } n \leq 1 \\
a_1^{n,s(n)} + \avgcoll X_{id}(k)  & \quad \text{if } n > 1 \\
\end{cases}
\]


\section{Tree-based protocols}

In a tree-based protocol there is no concept of \emph{slot}. What we need to compute is rather the number of \emph{nodes} in the tree that represents the execution of the protocol. Thus $X(n)$ in the context of tree-based protocols will be the total number of nodes in the tree generated by the execution of the protocol over a population of $n$ tags.

\subsection{Evaluation of the \QT{}/\BS{} protocol}

We assume a uniform distribution of the tag IDs, which allows us to cover the \QT{} and the \BS{} protocols at the same time.

The total number of nodes in a binary tree is recursively defined as the root plus the number of nodes in the left subtree plus the number of nodes in the right subtree. If $k$ tags are identified in the left subtree, then $n-k$ will be necessarily identified in the right subtree. This happens with probability $\binmassfunc{2}{n}{k}$, i.e. the probability that $k$ tags out of $n$ have the same prefix in their binary ID.

\[
X(n) =
\begin{cases}
1       & \quad \text{if } n \leq 1 \\
1 +  \sum\limits_{k=0}^n \binmassfunc{2}{n}{k} (X(k) + X(n-k))  & \quad \text{if } n > 1 \\
\end{cases}
\]

\subsubsection{Total number of identification rounds}

The only difference is that we do not count the root if $n > 1$, since we can't know if the first round is going to be single or empty. The base cases will tell us: if there are no more tags, we will get an empty round; if there is only one tag we will get one identification round.

\[
X_{id}(n) =
\begin{cases}
n       & \quad \text{if } n \leq 1 \\
\sum\limits_{k=0}^n \binmassfunc{2}{n}{k} (X_{id}(k) + X_{id}(n-k))  & \quad \text{if } n > 1 \\
\end{cases}
\]

\section{System Efficiency}

To estimate the System Efficiency of a protocol, i.e. compute $SE(n)$ for a given value of $n$, we need to know how many slots the protocol executes to identify $n$ tags.
This means we just need to compute the $X(n)$ formula (this can be done with the aid of numerical or even general-purpose programming language, see appendix \ref{chap:equations}).
For example, with the \TSA{} protocol we have that $X(n)/n=2.30115820392$ with $n=500$, that is the average number of slots needed to identify one tag. Note that we set $\alpha=1$ (i.e. $s(n)=n$) because we do know the number $n$ of tags. By applying the \ref{eq:se_n} equation we get the following value for the \TSA{} protocol executed over $n=500$ tags:

\[
 SE(n) = \frac{n}{X(n)} = 0.43456377675
\]


\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color, scale=0.9]
    set datafile separator ','
    set xlabel 'Tags'
    set ylabel 'System Efficiency'
    f(x) = x*100
    set xrange [f(1):f(10)]
    set xtics 100
    set yrange [0:1]
    set ytics 0.2
    set style line 1 pt 2 lc rgb 'red'
    set style line 2 pt 4 lc rgb '#00CC00'
    set style line 3 pt 12 lc rgb '#3399FF'
    plot 'csv/fsa_se.csv' using 1:2 with linespoint ls 1 title '\FSA', 'csv/tsa_se.csv' using 1:2 with linespoint ls 2 title '\TSA', 'csv/qt_se.csv' using 1:2 with linespoint ls 3 title '\QT/\BS'
    \end{gnuplot}
    \caption{System Efficiency comparison by varying the number of tags.}
    \label{fig:system-efficiency}
\end{figure}

Figure \ref{fig:system-efficiency} shows a comparison of the System Efficiency of the \QT{} (or \BS{}), \FSA{} and \TSA{} protocols. \TSA{} is clearly the winner, while \FSA{} and \QT{} are pretty close to each other.
