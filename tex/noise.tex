\chapter{Analytical Estimation of Noise Effects}
\label{chap:noise}

Chapter \ref{chap:performance} did not take into account the effects of the noise on the performance of the identification protocols. The model was always assuming that a collision can only happen when two tags choose the same slot. We will see in this chapter how the performance changes when we consider the impact of noise on RFID communications.

\section{Noise Model for ALOHA-based protocols}
\label{sec:noise-model-aloha}

% 8.5.3 Packet Structure
% Gen 2 is a packetized, reader-talks-first protocol, like Class 1 Generation 1. The reader
% chooses the forward-link and reverse-link parameters and communicates these choices to
% the tags using the opening symbols of each packet, which are known as a preamble when
% the reader is initiating an inventory operation, or a frame sync when any other command
% is sent. Preambles set tag (uplink) parameters for the remainder of an inventory session.
% Frame syncs contain only the reader timing information, to help tags stay synchronized
% during successive operations.

% In most collisions, the reader will still be able to extract
% a preamble and (erroneous) RN16. The reader will then ACK with an invalid random
% number, to which neither tag will reply

In a transmission model based on the \GenTwo{} standard, an identification slot requires that only one tag sends its RN16 number. Thus it is crucial to understand what happens to this particular packet in the presence of noise: a bit-error on the RN16 caused by presence of noise can happen either in the preamble or in the RN16 payload itself.\footnote{There might be an error both in the preamble and the payload, but if the reader discards the preamble, we don't need to care about possible errors in the payload.} This has different impacts on the estimated number of idle, identification and collision slots.

\subsection{Errors in the preamble}

Let's consider what happens in all possible scenarios when tags receive a \emph{Query} command from the reader.

\begin{itemize}
 \item No one replies, we get an idle slot.
 \item Only one tag sends its RN16. This would normally result in an identification slot, but if an error occurs in the preamble, the whole RN16 packet gets discarded by the reader and as result we get an idle slot instead.
 \item Multiple tags send their RN16 in the same slot, which would normally be classified as collision slot.
 Since the preamble is the same for every tag and the reader receives a single copy of it, if there is an error the reader discards the preamble and the slot is again considered as idle slot.
\end{itemize}

\subsection{Errors in the RN16}

Things are more complicated if the bit-error happens only in the RN16 payload, but \emph{not} in its preamble. RN16 do not carry CRC codes, thus the reader has no way to detect a corrupted RN16.

Let's consider again all the possible cases:

\begin{itemize}
 \item No one replies, we get an idle slot.
 \item Only one tag sends its RN16. If an error occurs in the RN16 payload, the reader will still send an \emph{ACK} packet for it. Since it will not get an EPC as reply, this time the identification slot becomes a collision slot rather than idle!
 \item Multiple tags send their RN16 in the same slot. The slot will be a collision slot in any case, because the reader will still see a (wrong) RN16 which will send in an \emph{ACK}.
\end{itemize}

\subsection{Expected number of slots}

Let $\preambleerr$ be the probability of error in the preamble and $\dataerr$ the probability of error in the payload's data (i.e. the RN16). We have that $\preambleerr + \dataerr + \noerror = 1$, where $\noerror$ is the probability of no errors (i.e. neither in the preamble nor in the payload). Let also $\anyerr = \preambleerr + \dataerr$ be the probability of any type of error.
The expected number of slots used so far does not take into account these probabilities, so we need to introduce new definitions. Let $A_k^{n,s(n)}$ be the new expected number of slots with exactly $k$ collisions.

\subsubsection{Idle slots}

The expected number of idle slots is now higher than before, because some of the identification and collision slots will become idle with probability \preambleerr:

\[
 A_0^{n,s(n)} = a_0^{n,s(n)} + a_1^{n,s(n)} \preambleerr + \sum_{k=2}^n a_k^{n,s(n)}  \preambleerr
\]

If we expand the equation we obtain:

\begin{align*}
A_0^{n,s(n)} &= \avgidle + \avgid \preambleerr \\
& \quad + \sum_{k=2}^n s(n) \binmassfunc{s(n)}{n}{k} \preambleerr \\
&= \left(1 - \frac{1}{s(n)}\right)^{n-1} \left(s(n) \left(1 - \frac{1}{s(n)}\right) + n \preambleerr\right) \\
& \quad + s(n) \preambleerr \sum_{k=2}^n \binmassfunc{s(n)}{n}{k} \\
&= \left(1 - \frac{1}{s(n)}\right)^{n-1} \left(s(n) - 1 + n \preambleerr \right) \\
& \quad + s(n) \preambleerr \sum_{k=2}^n \binmassfunc{s(n)}{n}{k}
\end{align*}

Note that $A_0^{n,s(n)} = a_0^{n,s(n)}$ if $\anyerr = 0$, which is what we would expect.

\subsubsection{Identification slots}

The expected numbers of identification slots will be lower than before, because in this new model an identification slot remains an identification slot with probability $\noerror = 1 - \preambleerr - \dataerr$:

\[
 A_1^{n,s(n)} = a_1^{n,s(n)} \noerror = a_1^{n,s(n)} - a_1^{n,s(n)} \preambleerr - a_1^{n,s(n)} \dataerr
\]

This is exactly the number of identification slots minus the ones that became idle, minus the ones that became collision slots. The expanded equation is:

\[
 A_1^{n,s(n)} = \AVGid
\]
Again, if $\anyerr = 0$ then $\noerror = 1$, which leads to $A_1^{n,s(n)} = a_1^{n,s(n)}$ as expected.

\subsubsection{Collision slots}

Finally, a collision slot remains so with probability $(1-\preambleerr)$. We also need to add the identification slots that became collision slots, thus the expected number of collision slots becomes:

\[
 A_{k \geq 2}^{n,s(n)} = a_1^{n,s(n)} \dataerr + \sum_{k=2}^n a_k^{n,s(n)} (1-\preambleerr)
\]
which expanded becomes:

\begin{align*}
A_{k \geq 2}^{n,s(n)} &= \avgid \dataerr \\
& \quad + \sum_{k=2}^n s(n) \binmassfunc{s(n)}{n}{k} \left(1 - \preambleerr \right) \\
&= \avgid \dataerr \\
& \quad + s(n) (1 -  \preambleerr) \sum_{k=2}^n  \binmassfunc{s(n)}{n}{k}
\end{align*}

As usual, if $\anyerr = 0$ we get:

\[
A_{k \geq 2}^{n,s(n)} = \sum_{k=2}^n a_k^{n,s(n)} = a_{k \geq 2}^{n,s(n)}
\]

\subsection{Evaluation of the \FSA{} protocol}

We begin by evaluating the \FSA{} protocol in our new model.
One big difference with the equations from chapter \ref{chap:performance} is that we replace all the $a_k^{n,s(n)}$ occurrences with $A_k^{n,s(n)}$.

We also need to tweak the base cases, since in this model the protocol doesn't necessarily ends when there is only one tag left.

\subsubsection{Total number of slots}
If there is only one tag, the protocol ends only if there is an identification or empty slot.
If there is a collision slot instead (which happens with probability \dataerr) the reader will issue another frame, which we need to count.

Note that if $\preambleerr = 1$, the equation would enter in an infinite loop because there are zero tags identified and we would recursively call the function without decrementing $n$.
The same would happen if $A_1^{n,s(n)} < 1$, since we would have $\floor{A_1^{n,s(n)}} = 0$ identifications.
To prevent this issue we abort the protocol in both cases (which is what the reader would do, since it would think that there are no more tags).
In all the other cases we always assume $\preambleerr < 1$.


\[
X(n) =
\begin{cases}
s(n)   & \quad \text{if } \preambleerr = 1 \\
s(n)   & \quad \text{if } A_1^{n,s(n)} < 1 \\
s(n)   & \quad \text{if }  n = 0 \\
s(n) + X(1)\dataerr   & \quad \text{if }  n = 1 \\
s(n) + X(n - \floor{A_1^{n,s(n)}})  & \quad \text{if } n > 1 \\
\end{cases}
\]

\subsubsection{Total number of idle slots}

The same reasoning we just described also holds for the equations for the total number of, respectively, idle and identification slots.

\[
X_{idle}(n) =
\begin{cases}
s(n)   & \quad \text{if } \preambleerr = 1 \\
s(n)   & \quad \text{if } A_1^{n,s(n)} < 1 \\
s(n)   & \quad \text{if } n=0 \\
A_0^{1,s(1)} + X_{idle}(1)\dataerr   & \quad \text{if }  n = 1 \\
A_0^{n,s(n)} +  X_{idle}(n - \floor{A_1^{n,s(n)}})  & \quad \text{if } n > 1 \\
\end{cases}
\]

\subsubsection{Total number of identification slots}

\[
X_{id}(n) =
\begin{cases}
0   & \quad \text{if } \preambleerr = 1 \\
0   & \quad \text{if } A_1^{n,s(n)} < 1 \\
0   & \quad \text{if }  n = 0 \\
A_1^{1,s(1)} + X_{id}(1)\dataerr   & \quad \text{if }  n = 1 \\
A_1^{n,s(n)} +  X_{id}(n - \floor{A_1^{n,s(n)}})  & \quad \text{if } n > 1 \\
\end{cases}
\]


\subsection{Evaluation of the \TSA{} protocol}

We know that in the \TSA{} protocol only the $k$ colliding tags that cause a collision slot participate in the child frame for that slot. However, in this new model there are also colliding slots which were supposed to be identification slots, i.e. there is only one tag involved. This means that the protocol will execute some child frames in which only one tag participates.

\subsubsection{Total number of slots}

As we did with \FSA{}, we add an additional base case for $\preambleerr=1$.
Note also how the normal base case only holds for $n=0$ now.
If there is only one tag, we can still get a collision. Thus we have the recursive evaluation for $n \geq 1$ rather than $n=1$ (check section \ref{sec:tsa-classic-evaluation}).

\[
X(n) =
\begin{cases}
s(n) & \quad \text{if } \preambleerr = 1 \\
s(n) & \quad \text{if } n = 0 \\
s(n) + \avgid X(1)\dataerr & \quad \text{if } n \geq 1 \\
+ \ s(n)(1- \preambleerr) \sum\limits_{k=2}^n p(s(n),n,k) X(k)
\end{cases}
\]

\subsubsection{Total number of idle slots}


\[
X_{idle}(n) =
\begin{cases}
s(n) & \quad \text{if } \preambleerr = 1 \\
s(n) & \quad \text{if } n = 0 \\
A_0^{n,s(n)} + \avgid X_{idle}(1)\dataerr & \quad \text{if } n \geq 1 \\
+ \ s(n)(1- \preambleerr) \sum\limits_{k=2}^n p(s(n),n,k) X_{idle}(k)
\end{cases}
\]


\subsubsection{Total number of identification slots}

\[
X_{id}(n) =
\begin{cases}
0 & \quad \text{if } \preambleerr = 1 \\
0 & \quad \text{if } n = 0 \\
A_1^{n,s(n)} + \avgid X_{id}(1)\dataerr & \quad \text{if } n \geq 1 \\
+ \ s(n)(1 - \preambleerr) \sum\limits_{k=2}^n p(s(n),n,k) X_{id}(k)
\end{cases}
\]

\section{Noise Model for tree-based protocols}

With tree-based protocols we don't have slots and the transmission model is not based on \GenTwo{}, but we still assume that we can have errors either in the preamble or in the actual payload transmitted by the tags who reply to the reader's query. These events happen respectively with probability \preambleerr{} and \dataerr{}.

\subsection{Evaluation of the \QT{} protocol}

\subsubsection{Total number of rounds}

As for \FSA{}, we need two different base cases: if there is one tag we could get more than one round, since we get a collision reply with probability \dataerr{}.
If there are multiple tags, we need to ignore the collisions that became empty rounds (which happens with probability \preambleerr).

\[
X(n) =
\begin{cases}
1       & \quad \text{if } n = 0 \\
1 + X(1)\dataerr & \quad \text{if } n = 1 \\
1 + (1-\preambleerr)\sum\limits_{k=0}^n \binom{n}{k} \left(\frac{1}{2}\right)^{k} \left(\frac{1}{2}\right)^{n-k} (X(k) + X(n-k))  & \quad \text{if } n > 1 \\
\end{cases}
\]

\subsubsection{Total number of identification rounds}

If there are no tags we will get zero identification rounds.
If there is only one tag, we can get either one identification round (with probability $1 - \preambleerr - \dataerr$), or a collision with probability \dataerr{} which will trigger another round.

\[
X_{id}(n) =
\begin{cases}
0       & \quad \text{if } n = 0 \\
1 - \preambleerr - \dataerr + X_{id}(1)\dataerr & \quad \text{if } n = 1 \\
(1 - \preambleerr)\sum\limits_{k=0}^n \binom{n}{k} \left(\frac{1}{2}\right)^{k} \left(\frac{1}{2}\right)^{n-k} (X_{id}(k) + X_{id}(n-k))  & \quad \text{if } n > 1 \\
\end{cases}
\]


\section{System Efficiency}

In this section we will present a series of plots in which we compare how the different protocols perform in presence of noise, with respect to their System Efficiency.
In particular, we focus again on the \FSA{}, \TSA{} and \QT{} protocols.

\subsection{Influence of probability of errors}

First of all, we analyze how the probability of errors \anyerr{} affects the System Efficiency of the protocols.

Figure \ref{fig:system-efficiency-noise-Perr} shows a comparison of the protocols by varying \anyerr{} from 0 to 0.9.

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color, scale=0.9]
    set datafile separator ','
    set xlabel 'Probability of errors'
    set ylabel 'System Efficiency'
    set xrange [0:0.9]
    set xtics 0.1
    set yrange [0:0.5]
    set ytics 0.1
    set style line 1 pt 4
    set style line 2 pt 5
    set style line 3 pt 2
    set style line 4 pt 13 lc rgb 'brown'
    plot 'csv/fsa_se_noise_100_Perr.csv' using 1:2 title '\FSA{} ($n=100$)' with linespoint ls 1, 'csv/fsa_se_noise_500_Perr.csv' using 1:2 title '\FSA{} ($n=500$)' with linespoint ls 2, 'csv/tsa_se_noise_500_Perr.csv' using 1:2 title '\TSA{} ($n=500$)' with linespoint ls 3, 'csv/qt_se_noise_500_Perr.csv' using 1:2 title '\QT{} ($n=500$)' with linespoint ls 4
    \end{gnuplot}
    \caption{System Efficiency comparison by varying $\anyerr$ $(\preambleerr=0.15 \cdot \anyerr)$.}
    \label{fig:system-efficiency-noise-Perr}
\end{figure}

The probabilities $\preambleerr$ and $\dataerr$ have been set to, respectively, the \SI{15}{\percent} and \SI{85}{\percent} of $\anyerr$ (since the preamble is usually much smaller than the 96 bits of the EPC data).\footnote{This will be the case for all the performance simulations in this chapter.}

The result of this analysis is that the bigger the probability of errors, the lower the System Efficiency.

The main finding from this simulation is that \FSA{} is the more affected by \anyerr, performing worse than \QT{}.

In the \FSA{} case, the performance simulation also shows that the higher the number of tags, the higher the System Efficiency.
This does not hold for the \TSA{} and the \QT{} protocols, instead: varying the number of tags results in very small changes in the System Efficiency values (which is why we only show a single $n=500$ dataset). %%% TODO: WHY???

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color, scale=0.9]
    set datafile separator ','
    set key outside horizontal
    set ylabel 'System Efficiency'
    set yrange [0:0.5]
    set ytics 0.05
    set boxwidth 0.7
    set style data histograms
    set style fill pattern
    plot 'csv/se_comparison_Perr.csv' using 2:xtic(1) title 'Classic' fs pattern 1, '' using 3 title '$\anyerr=0.01$' fs pattern 2, '' using 4 title '$\anyerr=0.05$' fs pattern 3, '' using 5 title '$\anyerr=0.1$' fs pattern 4, '' using 6 title '$\anyerr=0.3$' fs pattern 5
    \end{gnuplot}
    \caption{System Efficiency comparison between classic and noise models with $n = 500 $ ($\preambleerr=0.15 \cdot \anyerr$).}
    \label{fig:system-efficiency-comparison-Perr}
\end{figure}

The plot in figure \ref{fig:system-efficiency-comparison-Perr} shows more explicitly the comparison of the main protocols with their version in the \emph{classic} model, where we don't have any noise.
In this case the protocols were always executed with $n=500$ tags.

\section{Percentage of identification}

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color, scale=0.9]
    set datafile separator ','
    set key outside horizontal
    set xlabel 'Probability of errors'
    set ylabel 'Percentage of identification'
    set xrange [0:0.9]
    set xtics 0.1
    set yrange [0:1]
    set ytics 0.1
    set style line 1 pt 4
    set style line 2 pt 5
    set style line 3 pt 2
    set style line 4 pt 13 lc rgb 'brown'
    plot 'csv/fsa_id_noise_100_Perr.csv' using 1:2 title '\FSA{} ($n=100$)' with linespoint ls 1, 'csv/fsa_id_noise_500_Perr.csv' using 1:2 title '\FSA{} ($n=500$)' with linespoint ls 2, 'csv/tsa_id_noise_500_Perr.csv' using 1:2 title '\TSA{} ($n=500$)' with linespoint ls 3, 'csv/qt_id_noise_500_Perr.csv' using 1:2 title '\QT{} ($n=500$)' with linespoint ls 4
    \end{gnuplot}
    \caption{Percentage of identification comparison by varying $\anyerr$ $(\preambleerr=0.15 \cdot \anyerr)$.}
    \label{fig:identification-noise-Perr}
\end{figure}

So far we have only considered the System Efficiency metric, which only depends on the total number of slots (or nodes) executed by the protocol.
In this section we ask another question: do we identify all the tags? If not, how many of them do we lose and why?

We repeated our simulations by analyzing how many tags are actually identified and how this number is affected by the presence of noise.

To do so, we introduce the \emph{percentage of identification} as a new, intuitive metric:

\begin{equation}
ID(n) = \frac{X_{id}(n)}{n}
\end{equation}

This metric simply gives us the percentage of the identified tags over the $n$ available tags.

Clearly, in the classic analytical model we were always assuming $X_{id}(n)=n$, which gives us a percentage of identification equal to one.
We will show how this is not the case if there are transmission errors caused by presence of noise.

\subsection{Influence of probability of errors}

The plot in figure \ref{fig:identification-noise-Perr} is a variant of figure \ref{fig:system-efficiency-noise-Perr} where we plot the percentage of identification rather than the SE.
The most important finding is that the \FSA{} protocol shows the best percentage of identification, despite having the worst System Efficiency: even if the probability of errors is very high, the protocol is able to identify most of the tags.

This happens because in \FSA{} a tag can always participate to the next frame if it hasn't been identified yet, regardless of whether the reason was a normal collision or an error caused by presence of noise.
This is not the case for the \TSA{} or \QT{} protocols instead:

\begin{itemize}
 \item With \TSA{}, a tag is supposed to be eventually identified in the subtree caused by some collision. If that subtree is \emph{lost} because of noise errors, the tag is also doomed since it will not get another chance to be identified.
 \item With \QT{}, if there is an error on the preamble, the reader will ignore a whole subset of tags which share the same prefix, since they will never be queried again.
\end{itemize}


\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color, scale=0.9]
    set datafile separator ','
    set key outside horizontal
    set ylabel 'Percentage of identification'
    set yrange [0.5:1]
    set ytics 0.05
    set boxwidth 0.7
    set style data histograms
    set style fill pattern
    plot 'csv/id_comparison_Perr.csv' using 2:xtic(1) title 'Classic' fs pattern 1, '' using 3 title '$\anyerr=0.01$' fs pattern 2, '' using 4 title '$\anyerr=0.05$' fs pattern 3, '' using 5 title '$\anyerr=0.1$' fs pattern 4, '' using 6 title '$\anyerr=0.3$' fs pattern 5
    \end{gnuplot}
    \caption{Comparison of the percentage of identification between classic and noise models ($n = 500 $).}
    \label{fig:identification-comparison-Perr}
\end{figure}

Figure \ref{fig:identification-comparison-Perr} shows more clearly how \FSA{} performs with respect to the other protocols, even in the worst scenario ($\anyerr=0.5$).
Obviously, if there are no errors the reader will always identify all the tags (leading to a percentage of identification equal to one).

\subsection{Influence of the frame size}

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color, scale=0.9]
    set datafile separator ','
    set key outside horizontal
    set xtics rotate by -45
    set ylabel 'System Efficiency'
    set yrange [0:0.5]
    set ytics 0.1
    set boxwidth 0.7
    set style data histograms
    set style fill pattern
    plot 'csv/se_comparison_alpha_Perr.csv' using 2:xtic(1) title 'Classic' fs pattern 1, '' using 3 title '$\anyerr=0.03$' fs pattern 2, '' using 4 title '$\anyerr=0.1$' fs pattern 3, '' using 5 title '$\anyerr=0.5$' fs pattern 4
    \end{gnuplot}
    \caption{Comparison of the SE by varying $\alpha$ ($n = 500 $).}
    \label{fig:se-comparison-alpha-Perr}
\end{figure}


In figures \ref{fig:se-comparison-alpha-Perr} and \ref{fig:identification-comparison-alpha-Perr} we repeated our performance simulations by focusing on what happens when we use a value of $\alpha$ different than 1, which makes the frame smaller or bigger than the number of tags.

It is interesting to focus on the value of $\alpha=0.55$ if $\anyerr=0$: with \FSA{} we have a small loss of tags identified (figure \ref{fig:identification-comparison-alpha-Perr}), even if there are no errors.
This happens because if the frame is too small, we eventually reach a point where the few tags still to be identified always choose the same slots.
Since we abort the protocol if $A_1^{n,s(n)} < 1$, we loose those few tags.
The only thing that the reader could do is try again with a bigger frame.

On the other hand, \TSA{} maintains a percentage of identification close to one.
A smaller frame results in less idle slots and way more collision slots: since \TSA{} resolves the collisions as soon as they happen, this does not affect the functionality of the protocol.

\begin{figure}[!htb]
    \centering
    \begin{gnuplot}[terminal=epslatex, terminaloptions=color, scale=0.9]
    set datafile separator ','
    set key outside horizontal
    set xtics rotate by -45
    set ylabel 'Percentage of identification'
    set yrange [0.4:1]
    set ytics 0.1
    set boxwidth 0.7
    set style data histograms
    set style fill pattern
    plot 'csv/id_comparison_alpha_Perr.csv' using 2:xtic(1) title 'Classic' fs pattern 1, '' using 3 title '$\anyerr=0.03$' fs pattern 2, '' using 4 title '$\anyerr=0.1$' fs pattern 3, '' using 5 title '$\anyerr=0.5$' fs pattern 4
    \end{gnuplot}
    \caption{Comparison of the percentage of identification by varying $\alpha$ ($n = 500 $).}
    \label{fig:identification-comparison-alpha-Perr}
\end{figure}

\section{Conclusions}

The performance evaluation we made in this work shows how the presence of noise does have an impact on the performance of sequential protocols.

The most important finding of our simulations is that the \FSA{} protocol is the most resilient to communication errors.
Even if the probability of errors is very high, \FSA{} manages to identify most of the tags, at the expense of a low System Efficiency and high protocol latency.

On the other hand, if the probability of error is high, then the \TSA{} and \QT{} protocols have a better System Efficiency than \FSA{}, but they identify less tags than \FSA{}.
This does not mean that these protocols are useless. If high latency times are an issue but the reliability of identification is not so important, they may still be acceptable solutions.

The problem with \TSA{} and \QT{} is that both protocols are strictly designed for error-free channels, where a collision can only happen because multiple tags transmit at the same time.
This work instead suggests that new protocols based on \TSA{} or tree-based solutions should address the presence of noise in their design phase.
