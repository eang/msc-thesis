\chapter{Overview of Sequential Protocols}
\label{chap:sequentials}

In the past few years a number of protocols has been proposed to solve the problem of \emph{fast} identification of a set of $n$ tags. Identification of RFID tags in practice translates to \emph{medium access control} (MAC), i.e. anti-collision protocols such as ALOHA.
Collisions happen because RFID tags are not able to perform \emph{collision detection}, neither before nor during a transmission.

These protocols are also called \emph{sequential} protocols because there is only one reader with one antenna, which means that only one tag at a time can successfully be identified by the reader. There is also a category of more advanced \emph{concurrent} protocols, where the reader has more than one antenna and collisions are exploited rather than avoided, but they will be not be covered by this work.

\section{Tree-based VS ALOHA-based protocols}

All the sequential protocols are essentially either a variant of \emph{Tree}-based protocols or the Slotted ALOHA protocol. In general, the reader issues \emph{queries} to identify tags. A query can have three possible outcomes:

\begin{itemize}
 \item \emph{Single} reply: only one tag replied and the tag is successfully identified.
 \item More than one tag replies. This is called a \emph{collision}.
 \item No one replies. This is usually called an \emph{empty} reply.
\end{itemize}

Both families belong to the TDMA category of MAC protocols \cite{shih2006taxonomy}.


\section{Tree-based protocols}

In a tree-based MAC protocol, the set of tags to be identified is represented by the root of a binary tree. Once there is a collision, the set of tags is split into two children nodes. The splitting process goes on until a single tag, i.e. a (green) leaf as in figure \ref{fig:tree-based}, is correctly identified by the reader.
Different splitting policies are used by different tree-based protocols.



\begin{figure}[!htb]
    \centering
    \digraph[scale=0.5]{treebasedoverview}{
        node[label="", shape=circle, fontname=lmodern];
        b[label="X", style=filled, fillcolor=red, fontname=lmodern];
        c[label="X", style=filled, fillcolor=red];
        d[style=filled, fillcolor=green];
        e[label="X", style=filled, fillcolor=red];
        f[style=filled, fillcolor=green];
        g[style=filled, fillcolor=green];
        h[style=filled, fillcolor=green];
        i[style=filled, fillcolor=green];
        a -> {b, c};
        b -> {d, e};
        c -> {f, g};
        e -> {h, i};
    }
    \caption{An example of tree-based protocol run.}
    \label{fig:tree-based}
\end{figure}


\subsection{Binary Splitting}

In the Binary Splitting (\BS) protocol \cite{myung2006adaptive}, colliding tags are split by randomly incrementing a counter (maintained by each tag). The counter is initially set to zero and is changed depending on the outcome of the query (which is notified by the reader to the tags):

\begin{itemize}
 \item \emph{Identification}: only one tag has replied to the query. All the other tags decrement their counter by one.
 \item \emph{Collision}: more than one tag has replied. All these colliding tags increment their counter by either zero or one, chosen randomly (this is where the actual splitting happens). All the other (silent) tags increment their counter by one, instead.
 \item \emph{No answer}: all the tags decrement their counter by one.
\end{itemize}

Tags are allowed to reply only when their counter is zero. The protocol goes on until all tags are identified.

\subsection{Query Tree}

In the Query Tree (\QT) protocol \cite{law2000efficient}, the reader's query contains a binary string.
A tag can reply only if its (unique) ID starts with that string (prefix).
The reader initially sends the empty string, causing all tags to reply.
Then the protocol (and the splitting process) goes on by appending both a 0 and a 1 to every previous query string.

An example is shown in figure \ref{fig:query-tree} with a set of three tags whose IDs are 0100, 0111 and 1000. Note how every Query Tree is a full binary tree: each node has either zero (if there is an empty reply) or two children (if there is a collision reply).


\begin{figure}[!htb]
    \centering
    \digraph[scale=0.5]{querytree}{
        node[label="", fontname=lmodern];
        a[style=dashed];
        b[label="0", style=filled, fillcolor=red];
        c[label="1", style=filled, fillcolor=green];
        d[label="00", style=dashed];
        e[label="01", style=filled, fillcolor=red];
        f[label="010", style=filled, fillcolor=green];
        g[label="011", style=filled, fillcolor=green];
        a -> b[style=dashed];
        a -> c[style=dashed];
        b -> d[style=dashed];
        b -> e;
        e -> {f, g};
    }
    \caption{An example of the \QT{} protocol}
    \label{fig:query-tree}
\end{figure}

An improved version of \QT{} exists, called Query Tree Improved (\QTI), where the reader skips queries that are certainly going to cause a collision. For example, in \ref{fig:query-tree} we can skip the query 01 because no one replied to the query 00.

The performance of \QT{} depends on the distribution of the tag IDs. Assuming a uniform distribution, the \QT{} protocol becomes essentially just another way of doing Binary Splitting.

\subsection{Collision Tree}

The Collision Tree (\CT) protocol \cite{jia2010efficient} is a variant of the \QT{} protocol. In particular, is very similar to \QTI, as the main goal is reduce the number of queries that will surely result in a collision.
However, the approach taken is different. While in \QT{} the tag's reply contains the whole ID (if the prefix matches), in \CT{} tags send their ID without the prefix string from the reader. For example, if the reader's query is 0 and the tag ID is 0110, the tag will send only the 110 substring.

The next couple of children queries issued by the reader is built upon the previous query (i.e. the parent query in the tree) and the replies from the tag. In particular the reader will send the following two strings:

\[
\begin{cases}
  q_0 q_1 \dots q_k r_0 r_1 \dots r_{c-1} 0 \\
 q_0 q_1 \dots q_k r_0 r_1 \dots r_{c-1} 1 \\

\end{cases}
\]
where $q_i,r_i \in \{0, 1\}$, $q_0 q_1 \dots q_k$ is the parent query and $r_0 r_1 \dots r_{c-1}$ is the common prefix among all the tag responses (i.e. $r_c$ is the first different bit).

Figure \ref{fig:collision-tree} shows an example of execution of the protocol and the resulting tree. Edges represent the reader's query, nodes contains all the tags that replied to that query (leaf nodes being the tags that were identified).

\begin{figure}[!htb]
    \centering
    \digraph[scale=0.5]{collisiontree}{
        node[label="", fontname=lmodern];
        edge[fontname=lmodern];
        a[label="0001, 0010, 0011, 1110, 1111", style=dashed];
        b[label="0001, 0010, 0011", style=filled, fillcolor=red];
        c[label="1110, 1111", style=filled, fillcolor=red];
        d[label="0001", style=filled, fillcolor=green];
        e[label="0010, 0011", style=filled, fillcolor=red];
        f[label="1110", style=filled, fillcolor=green];
        g[label="1111", style=filled, fillcolor=green];
        h[label="0010", style=filled, fillcolor=green];
        i[label="0011", style=filled, fillcolor=green];
        a -> b[xlabel=0];
        a -> c[xlabel=1];
        b -> d[xlabel=000];
        b -> e[xlabel=001];
        e -> h[xlabel=0010];
        e -> i[xlabel=0011];
        c -> f[xlabel=1110];
        c -> g[xlabel=1111];
    }
    \caption{An example of the \CT{} protocol}
    \label{fig:collision-tree}
\end{figure}


\section{ALOHA-based Protocols}

Most of the protocols in this category are an evolution of the well known Slotted ALOHA protocol \cite{roberts1975aloha}.

Slotted ALOHA is an improved version of the basic ALOHA protocol.
Time is divided into fixed-size slots in which tags are allowed to reply to the reader's query. This requires a time synchronization between reader and tags, something that we don't need in the basic ALOHA.
Thanks to fixed-size slots, we reduce the high probability of collisions which is typical of ALOHA.

Depending on the number of tags that choose a given slot, the slot can be defined as:

 \begin{itemize}
  \item \emph{Idle slot}: if no tag chooses it.
  \item \emph{Identification slot}: if only one tag chooses it.
  \item \emph{Collision slot}: if more than one tag chooses it.
 \end{itemize}


\subsection{Framed Slotted Aloha}

In the Framed Slotted Aloha (\FSA{}) variant \cite{vogt2002efficient}, slots are grouped into \emph{frames}. A tag can choose only one slot in a given frame (whose length is advertised by the reader). In figure \ref{fig:fsa} there is an example of a 4-slots frame with two collision slots, one idle slot and one identification slot. If a tag collides in a frame, it will try again in the next one.

\begin{figure}[!htb]
    \centering
    \begin{tabular}{ | c | c | c | c | c | c | }
        \hline
        Downlink     & Query &           &  &         &            \\ \hline
        Uplink       &       & \cellcolor{red} Collision & Idle &  \cellcolor{red} Collision & \cellcolor{green} 11110101   \\ \hline
        Tag 10110010 &       & 10110010  & &           &            \\ \hline
        Tag 10100011 &       &           & & 10100011  &            \\ \hline
        Tag 10110011 &       & 10110011  & &           &            \\ \hline
        Tag 11110101 &       &           & &           & 11110101   \\ \hline
        Tag 10111010 &       &           & & 10111010  &            \\ \hline
    \end{tabular}
    \caption{An example of \FSA{} frame with 4 slots}
    \label{fig:fsa}
\end{figure}

\subsection{A fundamental problem: how many slots in a frame?}

How many slots should a frame have \emph{if we don't know how many tags we are going to identify}? There is a trade-off between the number of slots and the \emph{latency} of the protocol (total time required to identify all the tags). The less the slots, the more the collisions. On the other hand, the more the slots, the less the collisions but also the more the idle slots, which unfortunately waste a lot of time.

The whole point in using frames is to be able to set a proper frame size in order to reduce the number of collisions. \FSA{} does that by adapting the size of the next frame based on the outcome of the previous one: if we set an initial size for the tag population (\cite{bonuccelli2007instant} shows 128 as a good initial size), then we can estimate the number of colliding tags in a slot by subtracting the number of identification slots in the frame (which we do know). As the protocol goes on, the estimate $n$ of the actual number of tags is calculated with:

\begin{equation}
\label{eq:chebyshev}
\varepsilon(s(n), c_0, c_1, c_k) = \min_n \left|
    \begin{bmatrix}
    a_0^{n,s(n)} \\
    a_1^{n,s(n)} \\
    a_{k \geq 2}^{n,s(n)}
    \end{bmatrix}
    -
    \begin{bmatrix}
    c_0 \\
    c_1 \\
    c_k
    \end{bmatrix}
\right|
\end{equation}

where $a_k^{n,s(n)}$ is the expected number of slots with $k$ colliding tags in a frame of size $s(n)$, while $c_k$ is the number of slots with $k$ colliding tags observed in the previous frame. In practice, $n$ is the number that minimizes the difference between the expected and the actual number of idle, identification and colliding slots. Note that $a_{k \geq 2}^{n,s(n)} = \sum_{k=2}^n a_k^{n,s(n)}$ is the \emph{total} number of collision slots.

The expected number of slots with exactly $k$ collisions is given by a random variable $X \sim Bin(n, \frac{1}{s(n)})$:

\begin{equation}
  \label{eq:expected_slots}
 a_k^{n,s(n)} = s(n) f_X(k) = s(n) \binmassfunc{s(n)}{n}{k}
\end{equation}


where $f_X(k)=\mathbf{P}[X=k]$ is the probability mass function of $X$.

\subsection{EPC \GenTwo{}}

The identification protocol used by the EPC Class-1 Generation-2 (\GenTwo{}) standard \cite{standard1global} is based on \FSA{}. The reader just sends a parameter $Q$ at the beginning of each frame, whose size will be $2^Q$ slots. Tags randomly choose a number (called the \emph{slot counter}) in the range $[0, 2^Q)$: a tag replies only if it chooses 0, otherwise it waits for the next slot in which will decrease its slot counter. The process goes on until the slots counter becomes zero.
Frames are called \emph{Inventory Rounds} and are started by a \emph{Query} command, which contains a 4 bits field representing the $Q$ parameter ($Q \in [0,15]$). The \emph{Query} command can have three possible outcomes:

\begin{itemize}
 \item No tags reply. This results in an idle slot. The reader may issue another \emph{Query} to start the next round.
 \item One tag replies. The tag sends first its RN16 (a 16 bits random number), the reader acknowledges it with the \emph{ACK} command and finally the tag sends its 96 bits EPC value (plus CRC16, 16 bits of CRC) if the ACK contains the right RN16. The slot is classified as identification slot.
 \item More than one tag replies. The reader receives a waveform comprising of multiple RN16 and it will retrieve a wrong RN16. No tag will reply to the \emph{ACK} command for that RN16.
\end{itemize}


% TODO: does the standard say something about how Q should be set? YES, see Annex D!

\subsection{Tree Slotted Aloha}

The Tree Slotted Aloha (\TSA{}) protocol \cite{bonuccelli2007instant} combines the \FSA{} protocol with a tree-based approach.
For each collision slot, a new \emph{child} frame is issued by the reader and only the tags that replied in the colliding slot will participate in this child frame.
This can be implemented by broadcasting also the index of colliding slot that generated the frame, so that tags can detect if they need to participate to the child frame.
Figure \ref{fig:tsa} shows an example of execution of \TSA{}: slots marked with X, 1 and 0 are respectively collision, identification and idle slots. Note how the construction of the tree proceeds in depth-first order.

\begin{figure}[!htb]
    \centering
    \digraph[scale=0.5]{tsa}{
        node[shape=record, fontname=lmodern];
        a[label="<f0> X | <f1> X | <f2> 1 | <f3> 0 | ..."];
        b[label="<f0> 1 | <f1> X | <f2> X | <f3> X"];
        c[label="..."];
        d[label="<f0> 1 | <f1> 1"];
        e[label="<f0> X | <f1> 1"];
        f[label="<f0> 1 | <f1> 1"];
        g[label="<f0> 1 | <f1> 1"];
        a:f0 -> b;
        a:f1 -> c;
        b:f1 -> d;
        b:f2 -> e;
        b:f3 -> f;
        e:f0 -> g;
    }
    \caption{An example of the \TSA{} protocol}
    \label{fig:tsa}
\end{figure}

As in \FSA{}, the reader does not know how many tags chose the same colliding slot, but it knows how many identification slots occurred and it starts with an estimate of the number of tags. Thus, the size of the next child frame for a given colliding slot is set to the estimate of colliding tags (calculated as \FSA{} does) divided by the number of colliding slots.

The main strength of \TSA{} is that is solves collisions as soon as they happen, while in \FSA{} tags that did not collide between each other in a frame, can collide in the next frame. However, \cite{la2011anticollision} shows how the protocol suffers with big tag populations, for example 5000 tags.
\TSA{} also uses equation \ref{eq:chebyshev} to estimate $n$, but this number varies in the range $[c_1 + 2c_k, 2(c_1+c_k)]$.
The upper bound of this range has been defined based on simulations as described by \cite{bonuccelli2007instant}, but it happens to be unrealistic when $n$ is big.

This problem is address by the \emph{Dynamic} \TSA{} protocol (\emph{Dy\_TSA}) \cite{maselli2008dynamic}, where the knowledge about the tag population gained during the protocol execution is exploited to better estimate the number of tags. In particular, the size of a child frame is set according to the number of tags identified in the previous sibling frames.

\subsection{Binary Splitting Tree Slotted Aloha}

The Binary Splitting \TSA{} (\BSTSA{}) protocol \cite{la2011anticollision} is divided in two phases: in the first one, the set of tags is recursively divided into two smaller groups, which creates a binary tree similar to the tree-based protocols. Once the splitting step (which is only applied to the left child) reaches a leaf (group with only one tag), the second step starts. This step applies an instance of \TSA{} to the right sibling, recursively repeating the process by going up the tree. The idea is that both siblings have roughly the same size (which is half the size of the parent): since we know the size of the left sibling (thanks to the recursive splitting), we can properly size the \TSA{} frame for the right sibling. The next \TSA{} frame (applied to the upper right-sibling) will have twice the size of the current frame.
Figure \ref{fig:bstsa} shows an example of execution of the protocol. The left sibling nodes (colored with grey) are the ones \BS{} is applied to.
The white nodes (right siblings) represents instead the tags that participate to the \TSA{} step.

\begin{figure}[!htb]
    \centering
    \digraph[scale=0.5]{bstsa}{
        node[fontname=lmodern];
        edge[fontname=lmodern];
        a[label=n, style=filled, fillcolor=grey];
        b[label="n/2", style=filled, fillcolor=grey];
        c[label="n/2"];
        d[label="n/4", style=filled, fillcolor=grey];
        e[label="n/4"];
        f[label="n/8", style=filled, fillcolor=grey];
        g[label="n/8"];
        h[label=2, style=filled, fillcolor=grey];
        i[label=2];
        l[label=1, style=filled, fillcolor=grey];
        m[label=1];
        mm[shape=record,label="<f0>"];
        ii[shape=record,label="|"];
        gg[shape=record,label="|||"];
        ee[shape=record,label="|||||||"];
        a -> b[xlabel=BS];
        a -> c;
        b -> d[xlabel=BS];
        b -> e;
        d -> f[xlabel=BS];
        d -> g;
        f -> h[style=dotted, xlabel=BS];
        f -> i[style=dotted];
        h -> l[xlabel=BS];
        h -> m;
        m -> mm[style=dashed, label=TSA];
        i -> ii[style=dashed, label=TSA];
        g -> gg[style=dashed, label=TSA];
        e -> ee[style=dashed, label=TSA];
    }
    \caption{An example of the \BSTSA{} protocol}
    \label{fig:bstsa}
\end{figure}

\section{Physical Layer}

\subsection{Preamble}

The \GenTwo{} standard defines how the packet of the identification protocols (MAC layer) are transmitted by the physical layer.

Every packet from the MAC layer is encapsulated into a physical layer packet, which is made by a preamble and the MAC layer frame. The preamble is a fixed-size string that is used to synchronize reader and tags. The size of this string depends on the encoding of data used by the protocol and impacts the protocol performance. In the \GenTwo{} case we are talking about the \FSA{} protocol:

\begin{itemize}
 \item For communications from reader to tags, there is only one preamble before the first slot of a frame (i.e. before a \emph{Query} command). % source: section 6.3.1.2.8 in EPC

 Its size is negligible because there are many slots in a frame.
 \item On the other hand, every communication from the tag to the reader needs a preamble and in this case its overhead cannot be ignored.
\end{itemize}

\subsection{Transmission Time Model}

Different slot types last a different amount of time. In particular, idle slots last a fraction of identification and collision slots. In the latter types of slots, tags have to transmit their 96 bits with their tag ID. If the slot is idle instead, no one replies and a so-called \texttt{RX\_THRESHOLD} times out on the reader, which is then allowed to start the next slot in advance.

\section{Interference caused by noise}

So far the existing literature has always been assuming a noise-free environment (we are only aware of one exception in \cite{law2000efficient}). In these traditional scenarios, if there is an interference is because of either:

\begin{itemize}
 \item Collisions between tags.
 \item Collisions when a tag is being queried by multiple readers (which is called \emph{Multiple} \emph{Reader-to-Tag} \emph{interference}, \cite{engels2002reader}).
 \item Collisions between multiple readers that do not overlap with each other (\emph{Reader-to-Reader interference}, \cite{engels2002reader}).
\end{itemize}

These problems can be addressed for example with concurrent protocols.
However, in a traditional sequential scenario, interference can also happen because of the presence of \emph{noise}: either background noise or other signals in the same frequency range.
Since noise is not taken into account in the performance analysis of those protocols, their theoretical performances might not necessarily reflect the actual performance of a RFID system deployed in a noisy environment.

\section{Performance Metrics}

The following section describes the most common metrics used to evaluate an identification protocol. These metrics can be used for example in an analytical evaluation, where we would usually count the number of slots and take into account their temporal duration.

\subsection{Latency}

The latency of an identification protocol is defined as the total time required to identify all the tags. Given that idle and identification/collision slots have different durations, for simplicity the latency is usually defined as the time taken by all the idle slots plus the time taken by all the other slots.

\subsection{System Efficiency}

Given an RFID system, the System Efficiency (SE) is the ratio between the time spent by the system identifying tags and the total time spent by the system (i.e. the latency of the protocol). This is a very intuitive metric which tells us the \emph{throughput} of the protocol, i.e. how much time the system is doing useful work and how much time is being wasted instead. The SE can be defined in terms of time as we just did, but also in terms of slots:

\begin{equation}
\label{eq:se}
 SE = \frac{S_{id}}{S_{idle} + S_{id} + S_{coll}}
\end{equation}

where $S_{id}$ is the total number of identification slots (which is the number of tags identified by the system), $S_{idle}$ is the total number of idle slots and $S_{coll}$ the total number of collision slots.

\subsection{Time System Efficiency}

The SE metric is a very common metric, but does not take into consideration the fact the idle slots are shorter. \cite{la2011anticollision} proposed another metric called Time System Efficiency (TSE) which does reflect the different size of slots: slot times are weighted and the time of idle slots gets a lower weight. Formally we have:

\begin{equation}
\label{eq:tse}
 TSE = \frac{S_{id}}{\beta S_{idle} + S_{id} + S_{coll}}
\end{equation}
if an idle slot lasts a $\beta$ fraction of an identification/collision slot.

