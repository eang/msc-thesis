from scipy.special import binom
from sys import argv
from utils import memoized

# For compatibility with the signature used by other protocols
def X(n, l, P_pre, P_data, l_max):
    return X_impl(n, P_pre + P_data, l_max)


# Internal implementation
@memoized
def X_impl(n, p, l_max):

    if n <= 1:
        return l_max

    s1 = 0
    for k in range(1,n):
        s1 += binom(n, k) * 0.5**k * 0.5**(n-k) * X_impl(k, p, l_max)

    s2 = 0
    for k in range(1,n):
        s2 += binom(n, k) * 0.5**k * 0.5**(n-k) * X_impl(n-k, p, l_max)

    p_n = p**n + n*p**(n-1)*(1-p)

    return (1 + 0.5**(n-1)*X_impl(0, p, l_max)*(1-p_n) + (s1+s2)*(1-p_n)) / (1 - p_n - 0.5**(n-1)*(1-p_n))

if __name__ == "__main__":
    n = int(argv[1]) # number of tags
    p = float(argv[2]) # probability of failure
    l_max = int(argv[3])
    X_n = X_impl(n, p, l_max)
    print('QT^l performance:')
    print('QT^l number of nodes: {} * n'.format(X_n / n if n > 0 else X_n))
    print('QT^l SE: {} / {} = {}'.format(n, X_n, n / X_n))
