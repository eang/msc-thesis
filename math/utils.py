'''
Decorator that adds memoization to functions (single or multiple arguments).
Usage:

@memoized
def my_func(n):
    ...

'''
def memoized(func):
    cache = {}
    def new_func(*args):
        if args in cache:
             return cache[args]
        else:
             temp = cache[args] = func(*args)
             return temp
    return new_func
