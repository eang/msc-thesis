import fsa
import tsa
import qt
import sys

# Prints to stdout a CSV file with the TSE for the given protocol.
# Usage: python csv_tse.py <protocol>
# Example: python csv_tse.py fsa

protocol = sys.argv[1].upper()

if protocol == 'FSA':
    X = fsa.X
elif protocol == 'TSA':
    X = tsa.X

print('# n, TSE of {} protocol'.format(protocol))
for n in range(100, 1100, 100):
    print('{}, {}'.format(n, 'TODO'))
