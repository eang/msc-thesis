from scipy.special import binom

import sys

# number of slots in the frame
def s(n, alpha):
    return alpha * n if n > 0 else 1


def p(N, n, k):
    return binom(n, k) * (1 / N)**k * (1 - 1/N)**(n-k)



# Expected number of slots in which exactly k tags reply.
def a(n, k, alpha):
    N = s(n, alpha)
    return N * p(N, n, k)

# Expected number of collision slots.
def a_coll(n, alpha):
    sum = 0
    for k in range(2,n+1):
        sum += a(n, k, alpha)
    return sum


def A_0(n, P_pre, P_data, alpha):
    return a(n, 0, alpha) + a(n, 1, alpha)*P_pre + a_coll(n, alpha)*P_pre


def A_1(n, P_pre, P_data, alpha):
    return a(n, 1, alpha)*(1 - P_pre - P_data)

def A_coll(n, P_pre, P_data, alpha):
    return a(n, 1, alpha)*P_data + a_coll(n, alpha)*(1 - P_pre)



if __name__ == "__main__":
    n = int(sys.argv[1]) # number of tags
    P_pre = float(sys.argv[2]) if len(sys.argv) > 2 else 0.0
    P_data = float(sys.argv[3]) if len(sys.argv) > 3 else 0.0
    alpha = float(sys.argv[4]) if len(sys.argv) > 4 else 1
    a_idle = a(n, 0, alpha)
    a_id = a(n, 1, alpha)
    a_collisions = a_coll(n, alpha)
    print('Expected number of slots:')
    print('# idle slots: {}'.format(a_idle))
    print('# identification slots: {}'.format(a_id))
    print('# collision slots: {}'.format(a_collisions))
    print('TOTAL: {}'.format(a_idle + a_id + a_collisions))
    A_idle = A_0(n, P_pre, P_data, alpha)
    A_id = A_1(n, P_pre, P_data, alpha)
    A_collisions = A_coll(n, P_pre, P_data, alpha)
    print('Expected number of slots with P_pre = {}, P_data = {}:'.format(P_pre, P_data))
    print('# idle slots: {}'.format(A_idle))
    print('# identification slots: {}'.format(A_id))
    print('# collision slots: {}'.format(A_collisions))
    print('TOTAL: {}'.format(A_idle + A_id + A_collisions))
