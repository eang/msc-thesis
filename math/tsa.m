alpha = 1
n = 10

function pmf = p(N, n, k)
    pmf = nchoosek(n, k) * (1 / N)^k * (1 - 1/N)^(n-k);
endfunction

function y = X(n)

  alpha = 1;
  N = alpha * n;

  if (n == 1)
    y = N;
  else
    s = 0;
    for k = [2:n-1],
      s = s + p(N,n,k) * X(k)
    endfor
    y = (N + N*s) / (1 - N * (1/N)^n);
  endif
endfunction

tsa = X(n) / n
