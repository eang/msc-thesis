from math import ceil
from scipy.special import binom

import sys

# number of slots in the frame
def s(n, alpha):
    return alpha * n if n > 0 else 1


def p(N, n, k):
    return binom(n, k) * (1 / N)**k * (1 - 1/N)**(n-k)


# total number of slots in FSA
def X(n, P_pre, P_data, alpha):
    N = s(n, alpha)
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if P_err == 1 or n == 0:
        return N

    if n == 1:
        return N / (1 - P_data)

    A_1 = n * ((1 - 1/N)**(n-1)) * P_no_err

    if A_1 < 1:
        return N

    return N + X(n - int(A_1), P_pre, P_data, alpha)


# total number of idle slots in FSA
def X_idle(n, P_pre, P_data, alpha):
    N = s(n, alpha)
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if P_err == 1 or n == 0:
        return N

    if n == 1:
        return (N - 1 + P_pre) / (1 - P_data)

    sum = 0
    for k in range(2,n+1):
        sum += p(N, n, k)

    A_0 = (1 - 1/N)**(n-1) * (N-1 + n*P_pre) + N * P_pre * sum
    A_1 = n * ((1 - 1/N)**(n-1)) * P_no_err

    if A_1 < 1:
        return N

    return int(A_0) + X_idle(n - int(A_1), P_pre, P_data, alpha)



# total number of identification slots in FSA
def X_id(n, P_pre, P_data, alpha):
    N = s(n, alpha)
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if P_err == 1 or n == 0:
        return 0

    if n == 1:
        return (1 - P_pre - P_data) / (1 - P_data)

    A_1 = n * ((1 - 1/N)**(n-1)) * P_no_err

    if A_1 < 1:
        return 0

    return int(A_1) + X_id(n - int(A_1), P_pre, P_data, alpha)

if __name__ == "__main__":
    n = int(sys.argv[1]) # number of tags
    P_pre = float(sys.argv[2]) if len(sys.argv) > 2 else 0.0
    P_data = float(sys.argv[3]) if len(sys.argv) > 3 else 0.0
    alpha = float(sys.argv[4]) if len(sys.argv) > 4 else 1
    X_n = X(n, P_pre, P_data, alpha)
    X_idle_n = X_idle(n, P_pre, P_data, alpha)
    X_id_n = X_id(n, P_pre, P_data, alpha)
    print('FSA performance with P_pre = {}, P_data = {}, alpha = {}'.format(P_pre, P_data, alpha))
    print('FSA number of slots: {} ({} * n)'.format(X_n, X_n / n if n > 0 else X_n))
    print('FSA number of idle slots: {} ({} * n)'.format(X_idle_n, X_idle_n / n if n > 0 else X_idle_n))
    print('FSA number of identification slots: {} ({} * n)'.format(X_id_n, X_id_n / n if n > 0 else X_id_n))
    print('FSA SE: {}'.format(X_id_n / X_n))
