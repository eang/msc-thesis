from scipy.special import binom
from utils import memoized

import sys

def p(N, n, k):
    return binom(n, k) * (1 / N)**k * (1 - 1/N)**(n-k)

# total number of rounds in QT
@memoized
def X(n, P_pre, P_data):
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if n == 0:
        return 1

    if n == 1:
        return 1 / (1 - P_data)

    s1 = 0
    for k in range(1,n):
        s1 += p(2, n, k) * X(k, P_pre, P_data)

    s2 = 0
    for k in range(1,n):
        s2 += p(2, n, k) * X(n-k, P_pre, P_data)

    return (1 + (1/2)**(n-1) * X(0, P_pre, P_data) * (1-P_pre) + (1-P_pre)*(s1+s2)) / (1 - (1/2)**(n-1) * (1-P_pre))


# total number of identification rounds in QT
@memoized
def X_id(n, P_pre, P_data):
    P_err = P_pre + P_data
    P_no_err = 1 - P_err

    if n == 0:
        return 0

    if n == 1:
        return (1 - P_pre - P_data) / (1 - P_data)

    s1 = 0
    for k in range(1,n):
        s1 += p(2, n, k) * X_id(k, P_pre, P_data)

    s2 = 0
    for k in range(1,n):
        s2 += p(2, n, k) * X_id(n-k, P_pre, P_data)

    return ((1/2)**(n-1) * X_id(0, P_pre, P_data) * (1-P_pre) + (1-P_pre)*(s1+s2)) / (1 - (1/2)**(n-1) * (1-P_pre))


if __name__ == "__main__":
    n = int(sys.argv[1]) # number of tags
    P_pre = float(sys.argv[2]) if len(sys.argv) > 2 else 0.0
    P_data = float(sys.argv[3]) if len(sys.argv) > 3 else 0.0
    X_n = X(n, P_pre, P_data)
    X_id_n = X_id(n, P_pre, P_data)
    print('QT performance with P_pre = {}, P_data = {}'.format(P_pre, P_data))
    print('QT number of rounds: {} ({} * n)'.format(X_n, X_n / n if n > 0 else X_n))
    print('QT number of identification rounds: {} ({} * n)'.format(X_id_n, X_id_n / n if n > 0 else X_id_n))
    print('QT SE: {}'.format(X_id_n / X_n))
