repo=~/tesi/magistrale

echo 'Generating CSVs (classic model)...'
python $repo/math/csv_se.py fsa > $repo/csv/fsa_se.csv
python $repo/math/csv_se.py tsa > $repo/csv/tsa_se.csv
python $repo/math/csv_se.py qt > $repo/csv/qt_se.csv

echo 'Generating CSVs (noise, P_err)...'
python $repo/math/csv_se_noise_Perr.py fsa 100 > $repo/csv/fsa_se_noise_100_Perr.csv
python $repo/math/csv_se_noise_Perr.py fsa 500 > $repo/csv/fsa_se_noise_500_Perr.csv
python $repo/math/csv_se_noise_Perr.py tsa 500 > $repo/csv/tsa_se_noise_500_Perr.csv
python $repo/math/csv_se_noise_Perr.py qt 500 > $repo/csv/qt_se_noise_500_Perr.csv

python $repo/math/csv_id_noise_Perr.py fsa 100 > $repo/csv/fsa_id_noise_100_Perr.csv
python $repo/math/csv_id_noise_Perr.py fsa 500 > $repo/csv/fsa_id_noise_500_Perr.csv
python $repo/math/csv_id_noise_Perr.py tsa 500 > $repo/csv/tsa_id_noise_500_Perr.csv
python $repo/math/csv_id_noise_Perr.py qt 500 > $repo/csv/qt_id_noise_500_Perr.csv

echo 'Generating SE comparisons...'
python $repo/math/csv_se_comparison_Perr.py 500 > $repo/csv/se_comparison_Perr.csv
python $repo/math/csv_se_comparison_alpha_Perr.py 500 > $repo/csv/se_comparison_alpha_Perr.csv

echo 'Generating Percentage of Identification comparisons...'
python $repo/math/csv_id_comparison_Perr.py 500 > $repo/csv/id_comparison_Perr.csv
python $repo/math/csv_id_comparison_alpha_Perr.py 500 > $repo/csv/id_comparison_alpha_Perr.csv
